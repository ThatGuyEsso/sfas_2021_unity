﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
//Scriptable object to store decisions made in game
[Serializable]
[CreateAssetMenu(menuName = "Decision Data", fileName = "Assets/Data/Decision.asset")]
public class DecisionData : ScriptableObject
{
    [SerializeField]
    private List<Consequence> _consequences;//List of every story beat


    //Find a consequence by its ID
    public Consequence GetConsequenceById(int id)
    {
        return _consequences.Find(c => c.ID == id);
    }

    //Find a consequence by its name
    public Consequence GetConsequenceByName(string name)
    {
        return _consequences.Find(c => c.ConsequenceName == name);
    }
    public void ResetAllDecisions()
    {
        foreach(Consequence consequence in _consequences)
        {
            consequence.Reset();
        }
    }

    public void GenerateConsequenceID()
    {
        for(int i =0; i < _consequences.Count; i++)
        {
            _consequences[i].SetID(i);
        }
    }
}
