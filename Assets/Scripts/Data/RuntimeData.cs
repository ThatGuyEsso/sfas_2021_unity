﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Scriptable object to store decisions made in game
[CreateAssetMenu(menuName = "Runtime Data", fileName = "Assets/Data/Runtime.asset")]
public class RuntimeData : ScriptableObject
{
    public bool isInitBoot;
}
