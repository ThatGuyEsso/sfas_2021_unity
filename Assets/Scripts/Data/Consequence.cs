﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Consequence
{
    //name of Consequence so it can be identfied more easily
    [SerializeField] private string _consequenceName;

    //unique _id of consequence 
    [SerializeField] private int _id;

    //Shows wether the consequence has been triggered yet
    [SerializeField] private bool _isTriggered = false;



    //Consequence delegate. If this delegate corrosponding delegate is triggered
    public event ConsequenceDelegate OnConsequenceTriggered;
    public delegate void ConsequenceDelegate();
    public void TriggerConsequence()
    {
        _isTriggered = true;
        OnConsequenceTriggered?.Invoke();
    }

    //reset decison when game is restarted
    public void Reset()
    {
        _isTriggered = false;
    }



    //Getters for C# operations
    public string ConsequenceName { get { return _consequenceName; } }
    public bool IsTriggered { get { return _isTriggered; } }
    public int ID { get { return _id; } }

    //All purpose getters
    public bool GetIsTriggered() { return _isTriggered; }
    public int GetID() { return _id; }
    public string GetConsequenceName() { return _consequenceName; }

    public void SetID(int newID) { _id= newID; }
    public void SetIsTriggered(bool isTriggered) { _isTriggered = isTriggered; }
}
