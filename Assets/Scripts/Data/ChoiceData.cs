﻿using System;
using UnityEngine;

//The type of trigger the choice is
public enum ChoiceTriggerType
{
    Null,
    RestartGame,
    CheckPoint,
    MainMenu,
    Quit
};
[Serializable]
public class ChoiceData
{
    [SerializeField] private string _text;
    [SerializeField] private int _beatId;
    [SerializeField] private string _consequenceName; //What Consequence it will trigger
    [SerializeField] private bool _needsValidation = false;//check if player can make this choice
    [SerializeField] private int _validBeatID;
    [SerializeField] private int _invalidBeatID;
    [SerializeField] private string _dependentConsequence; //What consequence will make it valid
    [SerializeField] private string _validConsequence; //What a consequence will trigger if choice is valid
    [SerializeField] private string _invalidConsequence; //What a consequence will trigger if choice is invalid
    [SerializeField] private ChoiceTriggerType _triggerType = ChoiceTriggerType.Null;
    public string DisplayText { get { return _text; } }
    public int NextID { get { return _beatId;}}
    public int ValidID { get { return _validBeatID; } }
    public int InvalidID { get { return _invalidBeatID; } }

    public ChoiceTriggerType TriggerType { get { return _triggerType; } }

    public string ConsequenceName { get { return _consequenceName; } }
    public string ValidConsequenceName { get { return _validConsequence; } }
    public string InvalidConsequenceName { get { return _invalidConsequence; } }
    public string DependentConsequence { get { return _dependentConsequence; } }
    public bool NeedsValidation { get { return _needsValidation; } }
}
