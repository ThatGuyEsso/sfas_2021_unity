﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(menuName = "User Settings", fileName = "Assets/Data/UserSettings.asset")]
public class UserSettings : ScriptableObject
{
    public float _mouseSensitivity;

}
