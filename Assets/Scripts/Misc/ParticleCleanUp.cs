﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCleanUp : MonoBehaviour
{
    protected ParticleSystem _system;

    protected virtual void Awake()
    {
        //Get particlesytem
        _system = gameObject.GetComponent<ParticleSystem>();
        StartCoroutine(WaitTillDestroy());
    }


    protected IEnumerator WaitTillDestroy()
    {
        //wait till particle system ends
        while (_system.IsAlive()) {
            yield return null;
        }
        //destroy gameobject after X time
        Destroy(gameObject, 0.5f);
    }
}
