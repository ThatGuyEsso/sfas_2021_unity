﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioListenerSwitch : MonoBehaviour
{
    [SerializeField] private Genres _activationGenre;//genre where Audio listener is active
    private AudioListener _listener;
    [SerializeField] private bool _startActive;
    bool isInitiated;
    private void Awake()
    {
        _listener = gameObject.GetComponent<AudioListener>();
        if (_startActive)
        {
            _listener.enabled = true;
        }
        else
        {
            _listener.enabled = false;
        }
    }

    public void InitSwitch()
    {
        GenreManager._instance.OnGenreChange += EvaluateNewGenre;
        GenreManager._instance.OnDirectChange += EvaluateNewGenre;
        isInitiated = true;
    }

    private void EvaluateNewGenre(Genres newGenre)
    {

        if (newGenre == _activationGenre)
        {
            _listener.enabled = true;
        }
        else
        {
            _listener.enabled = false;
        }
    }


    private void OnDestroy()
    {
        if (isInitiated)
        {
            GenreManager._instance.OnGenreChange -= EvaluateNewGenre;
            GenreManager._instance.OnDirectChange -= EvaluateNewGenre;
        }

    }
}
