﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingParticleCleanUp : ParticleCleanUp
{
    protected override void Awake()
    {
        _system = gameObject.GetComponent<ParticleSystem>();
    }


    public void CleanUpVFX()
    {
        _system.Stop();
        StartCoroutine(WaitTillDestroy());
    }
}
