﻿public enum SceneIndex
{
    ComputerScene =0,
    TitleScreenScene=1,
    WorldScene =2,
    ComputerScreenScene = 3,
    CreditScene=4
};