﻿using UnityEngine;
using UnityEditor;

public class StoryEditor : EditorWindow
{
    private enum View { List, Beat }

    private Vector2 _scroll = new Vector2();
    private int _currentIndex = -1;
    private View _view;

    [MenuItem("SFAS/Show Story Editor")]
    public static void ShowStoryEditor()
    {
        GetWindow(typeof(StoryEditor));
    }

    //sets up gui
    void OnGUI()
    {
        StoryData data = StoryData.LoadData();//Get story data from editor
        SerializedObject dataObj = new SerializedObject(data);//turn data in to an object
        SerializedProperty beatList = dataObj.FindProperty("_beats");//get beats

        //note* look up editor api 
        EditorGUILayout.BeginVertical();
        _scroll = EditorGUILayout.BeginScrollView(_scroll);


        if (_view == View.Beat && _currentIndex != -1)
        {
            OnGUI_BeatView(beatList, _currentIndex);
        }
        else
        {
            OnGUI_ListView(beatList);
        }

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();

        dataObj.ApplyModifiedProperties();
    }
    //defines list view
    private void OnGUI_ListView(SerializedProperty beatList)
    {
        EditorGUILayout.BeginVertical();

        if (beatList.arraySize == 0)
        {
            AddBeat(beatList, 1, "First Story Beat");
        }

        for (int count = 0; count < beatList.arraySize; ++count)
        {
            SerializedProperty arrayElement = beatList.GetArrayElementAtIndex(count);
            SerializedProperty choiceList = arrayElement.FindPropertyRelative("_choices");
            SerializedProperty text = arrayElement.FindPropertyRelative("_text");
            SerializedProperty id = arrayElement.FindPropertyRelative("_id");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(id.intValue.ToString());

            if (GUILayout.Button("Edit"))
            {
                _view = View.Beat;
                _currentIndex = count;
                break;
            }

            if (GUILayout.Button("Delete"))
            {
                beatList.DeleteArrayElementAtIndex(count);
                break;
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(text);
            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndVertical();
    }
    //defines how beats are displayed on the gui
    private void OnGUI_BeatView(SerializedProperty beatList, int index)
    {
        SerializedProperty arrayElement = beatList.GetArrayElementAtIndex(index);
        SerializedProperty choiceList = arrayElement.FindPropertyRelative("_choices");
        SerializedProperty text = arrayElement.FindPropertyRelative("_text");
        SerializedProperty id = arrayElement.FindPropertyRelative("_id");
        //To easily toggle if beat leads to a transition
        SerializedProperty isTrigger = arrayElement.FindPropertyRelative("_isTransitionTrigger");

        EditorGUILayout.BeginVertical();

        EditorGUILayout.LabelField("Beat ID: " + id.intValue.ToString());

        //Displays wether beat is a trigger to a transition
        text.stringValue = EditorGUILayout.TextArea(text.stringValue, GUILayout.Height(200));

        GUILayout.Label("\nIs Transition Trigger");
        isTrigger.boolValue = EditorGUILayout.Toggle(isTrigger.boolValue,GUILayout.Height(10f), GUILayout.Width(10f));
      
        OnGUI_BeatViewDecision(choiceList, beatList);

        EditorGUILayout.EndVertical();

        if (GUILayout.Button("Return to Beat List", GUILayout.Height(50)))
        {
            _view = View.List;
            _currentIndex = -1;
        }
    }
    //defines gow beats decisions are displayed
    private void OnGUI_BeatViewDecision(SerializedProperty choiceList, SerializedProperty beatList)
    {
        EditorGUILayout.BeginHorizontal();

        for (int count = 0; count < choiceList.arraySize; ++count)
        {
            OnGUI_BeatViewChoice(choiceList, count, beatList);
        }

        if (GUILayout.Button((choiceList.arraySize == 0 ? "Add Choice" : "Add Another Choice"), GUILayout.Height(100)))
        {
            int newBeatId = FindUniqueId(beatList);
            AddBeat(beatList, newBeatId);
            AddChoice(choiceList, newBeatId);
        }

        EditorGUILayout.EndHorizontal();
    }

    private void OnGUI_BeatViewChoice(SerializedProperty choiceList, int index, SerializedProperty beatList)
    {
        SerializedProperty arrayElement = choiceList.GetArrayElementAtIndex(index);
        SerializedProperty triggerType = arrayElement.FindPropertyRelative("_triggerType");
        SerializedProperty text = arrayElement.FindPropertyRelative("_text");
        SerializedProperty beatId = arrayElement.FindPropertyRelative("_beatId");

        EditorGUILayout.BeginVertical();
        GUILayout.Label("Trigger Type");
        string[] dropDown = { "Null", "RestartGame", "CheckPoint", "MainMenu", "Quit" };
        triggerType.enumValueIndex = EditorGUILayout.Popup(triggerType.enumValueIndex, dropDown, GUILayout.Width(100f));
        switch (triggerType.enumValueIndex)//What type of choice
        {
            case 0://"Null" No transition related to choice
             
                SerializedProperty needsValidation = arrayElement.FindPropertyRelative("_needsValidation");
                GUILayout.Label("\nChoice text");
                text.stringValue = EditorGUILayout.TextArea(text.stringValue, GUILayout.Height(50));
                GUILayout.Label("\nChoice needs validation");
                needsValidation.boolValue = EditorGUILayout.Toggle(needsValidation.boolValue, GUILayout.Height(10f), GUILayout.Width(10f));

                if (needsValidation.boolValue == true)
                {
                    SerializedProperty validBeatID = arrayElement.FindPropertyRelative("_validBeatID");
                    SerializedProperty invalidBeatID = arrayElement.FindPropertyRelative("_invalidBeatID");
                    SerializedProperty validConsequence = arrayElement.FindPropertyRelative("_validConsequence");
                    SerializedProperty invalidConsequence = arrayElement.FindPropertyRelative("_invalidConsequence");
                    SerializedProperty dependentConsequence = arrayElement.FindPropertyRelative("_dependentConsequence");

                    //Consequence evaluated
                    GUILayout.Label("Dependent Consequence");
                    dependentConsequence.stringValue = EditorGUILayout.TextArea(dependentConsequence.stringValue, GUILayout.Height(20f), GUILayout.Width(200f));

                    EditorGUILayout.LabelField("If valid Leads to Beat ID: " + validBeatID.intValue.ToString());
                    //Allows beat id to be modified
                    validBeatID.intValue = EditorGUILayout.IntField(validBeatID.intValue, GUILayout.Height(20), GUILayout.Width(30));
                    GUILayout.Label("Valid Choice Consequence");
                    validConsequence.stringValue = EditorGUILayout.TextArea(validConsequence.stringValue, GUILayout.Height(20f), GUILayout.Width(200f));
                    if (GUILayout.Button("Go to Beat", GUILayout.Height(20f), GUILayout.Width(100f)))
                    {
                        _currentIndex = FindIndexOfBeatId(beatList, validBeatID.intValue);
                        GUI.FocusControl(null);
                        Repaint();
                    }
                    EditorGUILayout.LabelField("If invalid Leads to Beat ID: " + invalidBeatID.intValue.ToString());
                    //Allows beat id to be modified
                    invalidBeatID.intValue = EditorGUILayout.IntField(invalidBeatID.intValue, GUILayout.Height(20), GUILayout.Width(30));
                    //Displays allows setting the consequence name
                    GUILayout.Label("Invalid Choice Consequence");
                    invalidConsequence.stringValue = EditorGUILayout.TextArea(invalidConsequence.stringValue, GUILayout.Height(20f), GUILayout.Width(200f));

                    if (GUILayout.Button("Go to Beat", GUILayout.Height(20f), GUILayout.Width(100f)))
                    {
                        _currentIndex = FindIndexOfBeatId(beatList, invalidBeatID.intValue);
                        GUI.FocusControl(null);
                        Repaint();
                    }

                }
                else
                {
                  
                    SerializedProperty consequence = arrayElement.FindPropertyRelative("_consequenceName");
                    EditorGUILayout.LabelField("Leads to Beat ID: " + beatId.intValue.ToString());
                    //Allows beat id to be modified
                    beatId.intValue = EditorGUILayout.IntField(beatId.intValue, GUILayout.Height(20), GUILayout.Width(30));

                    //Displays allows setting the consequence name
                    GUILayout.Label("Choice Consequence");
                    consequence.stringValue = EditorGUILayout.TextArea(consequence.stringValue, GUILayout.Height(20f), GUILayout.Width(200f));


                    if (GUILayout.Button("Go to Beat"))
                    {
                        _currentIndex = FindIndexOfBeatId(beatList, beatId.intValue);
                        GUI.FocusControl(null);
                        Repaint();
                    }
                }

                break;

            case 1://Restart game completely
                GUILayout.Label("\nChoice text");
                text.stringValue = EditorGUILayout.TextArea("Restart journey from the beginning", GUILayout.Height(20));
                beatId.intValue = 1;
                break;
            case 2://Restart from last checkpoint   
                GUILayout.Label("\nChoice text");
                text.stringValue = EditorGUILayout.TextArea("Restart Journey from the last Checkpoint", GUILayout.Height(20));
            
                break;
            case 3: //return to main menu
                GUILayout.Label("\nChoice text");
                text.stringValue = EditorGUILayout.TextArea( "Return to title screen", GUILayout.Height(20));
            
                break;
            case 4: //Quit application
                GUILayout.Label("\nChoice text");
                text.stringValue = EditorGUILayout.TextArea("Quit to desktop", GUILayout.Height(20));
                break;

        }

        EditorGUILayout.EndVertical();
    }

    //finds beat with unique ID
    private int FindUniqueId(SerializedProperty beatList)
    {
        int result = 1;

        while (IsIdInList(beatList, result))
        {
            ++result; 
        }

        return result;
    }

    private bool IsIdInList(SerializedProperty beatList, int beatId)
    {
        bool result = false;

        for (int count = 0; count < beatList.arraySize && !result; ++count)
        {
            SerializedProperty arrayElement = beatList.GetArrayElementAtIndex(count);
            SerializedProperty id = arrayElement.FindPropertyRelative("_id");
            result = id.intValue == beatId;
        }

        return result;
    }

    private int FindIndexOfBeatId(SerializedProperty beatList, int beatId)
    {
        int result = -1;

        for (int count = 0; count < beatList.arraySize; ++count)
        {
            SerializedProperty arrayElement = beatList.GetArrayElementAtIndex(count);
            SerializedProperty id = arrayElement.FindPropertyRelative("_id");
            if (id.intValue == beatId)
            {
                result = count;
                break;
            }
        }

        return result;
    }

    private void AddBeat(SerializedProperty beatList, int beatId, string initialText = "New Story Beat")
    {
        int index = beatList.arraySize;
        beatList.arraySize += 1;
        SerializedProperty arrayElement = beatList.GetArrayElementAtIndex(index);
        SerializedProperty text = arrayElement.FindPropertyRelative("_text");
        SerializedProperty id = arrayElement.FindPropertyRelative("_id");

        text.stringValue = initialText;
        id.intValue = beatId;
    }

    private void AddChoice(SerializedProperty choiceList, int beatId, string initialText = "New Beat Choice")
    {
        int index = choiceList.arraySize;
        choiceList.arraySize += 1;
        SerializedProperty arrayElement = choiceList.GetArrayElementAtIndex(index);
        SerializedProperty text = arrayElement.FindPropertyRelative("_text");
        SerializedProperty nextId = arrayElement.FindPropertyRelative("_beatId");

        text.stringValue = initialText;
        nextId.intValue = beatId;
    }
}
