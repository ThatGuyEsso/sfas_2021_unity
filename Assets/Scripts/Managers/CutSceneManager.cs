﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class CutSceneManager : BaseManager
{
    public static CutSceneManager _instance;
    [SerializeField] private TimelineAsset _intro;
    [SerializeField] private PlayableDirector _director;
    private void Awake()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
       
    }

    public override void Init()
    {
        BindToGameStateManager();
        _director = gameObject.GetComponent<PlayableDirector>();
      
    }


    private void PlayIntro()
    {
        _director.playableAsset = _intro;
        _director.Play();
        StartCoroutine(WaitTrackEnd());
    }



    protected override void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.InitIntro:
                PlayIntro();

                break;
        }
    }


    private IEnumerator WaitTrackEnd()
    {
        //wait for duration of clip
        yield return new WaitForSeconds((float)_director.duration);
        _director.Stop();
        GameStateManager._instance.BeginNewState(GameState.IntroEnd);
        
    }
}