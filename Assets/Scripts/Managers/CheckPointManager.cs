﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : BaseManager
{
    public static CheckPointManager _instance;
    [SerializeField] private Transform _spawn;
    [SerializeField] private List<CheckPoint> _checkPoints;
    [SerializeField] private Transform _fpCharacterTrans;

    [SerializeField] private CheckPoint _currentCheckPoint;

    private void Awake()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        BindToGameStateManager();
    }
    public override void Init()
    {

        //First person character
        _fpCharacterTrans = FindObjectOfType<FirstPersonBehaviour>().transform;
        //Bind to game state manager
      
        //Init checkpoints
        InitCheckPoints();
        BindToCheckPointActivation();

    }


    protected override void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
      
            case GameState.Resetting:
                ResetJourney();
                InitCheckPoints();
                break;
            case GameState.ResettingFromCheckPoint:
                ResetFromActiveCheckPoint();


                break;
        }
    }

    private void ResetJourney()
    {
        _fpCharacterTrans.position = _spawn.position;
        _fpCharacterTrans.forward = _spawn.forward;
    }

    private void SetCurrentCheckPoint(CheckPoint newCheckPoint)
    {
        _currentCheckPoint = newCheckPoint;
        foreach(CheckPoint checkPoint in _checkPoints)
        {
            if (checkPoint != _currentCheckPoint) checkPoint.Deactivate();
        }
    }
    private void ResetFromActiveCheckPoint()
    {
        //if there is an active checkpoint
        if (_currentCheckPoint !=false)
        {
            _fpCharacterTrans.position = _currentCheckPoint.transform.position;
            _fpCharacterTrans.forward = _currentCheckPoint.transform.forward;
            _currentCheckPoint.RestoreDecisions();
        }
        else
        {
            //no active check point so start from initial spawn
            ResetJourney();
        }
    }
    
    private void BindToCheckPointActivation()
    {
        foreach(CheckPoint checkPoint in _checkPoints){
            checkPoint.OnActivateCheckPoint += SetCurrentCheckPoint;
        }

    }
    private void InitCheckPoints()
    {
        foreach (CheckPoint checkPoint in _checkPoints)
        {
            checkPoint.Init();
        }
    }
}
