﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseManager : MonoBehaviour
{
    protected bool isInitiated =false;
    public virtual void Init()
    {
        isInitiated = true;
        //each manager have their own specific initialisation
    }
    //Become a delegate of the game state manager
    public void BindToGameStateManager()
    {
    
        GameStateManager._instance.OnStateChange += EvaluateNewState;
    }

    protected virtual void EvaluateNewState(GameState newState)
    {
        //Empty as managers will have their own implementation
    }


    //unbind incase of disable or destroy
    protected void OnDisable()
    {
        if (!isInitiated) return;
        GameStateManager._instance.OnStateChange -= EvaluateNewState;
    }

    protected virtual void OnDestroy()
    {
        if (!isInitiated) return;
        GameStateManager._instance.OnStateChange -= EvaluateNewState;
    
    }
}
