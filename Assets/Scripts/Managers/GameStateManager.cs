﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Meta Game States (state/phase of application to handle intiation)
public enum GameState
{
    InitIntro,
    IntroEnd,
    InitManagers,
    LoadTitleScreen,
    TitleScreen,
    CreditScreen,
    InitGame, 
    GameRunning,
    GameWin,
    GameFail,
    Resetting,
    ResettingFromCheckPoint,
    GameReset,
    QuitGame
};
public class GameStateManager : MonoBehaviour
{
    //static instance
    public static GameStateManager _instance;

    //manager prefabs to dynamically spawn in 
    [SerializeField]
    private List<GameObject> managerPrefabs;
    [SerializeField] private RuntimeData runtimeData;
    //Current gamestate of the game
    public static GameState _currGameState;

    //Delegates
    public event NewGameStateDelegate OnStateChange;
    public delegate void NewGameStateDelegate(GameState newState);

    public event ResetGameDelegate OnGameReset;
    public delegate void ResetGameDelegate();
    private void Awake()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        //persist throughout levels
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        //Hide cursor
        Cursor.visible = false;

      
        CutSceneManager._instance.Init();
        _currGameState = GameState.InitIntro;

        //call gameState
        OnStateChange?.Invoke(_currGameState);

    }
    //Evaluate new state and begin new action depending on it
    public void BeginNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.IntroEnd:
                SetUp();

                break;
            case GameState.InitGame:
                _currGameState = newState;
              
                 OnStateChange?.Invoke(_currGameState);
                CheckPointManager._instance.Init();

                _currGameState = GameState.GameRunning;
                OnStateChange?.Invoke(_currGameState);
                break;

            case GameState.TitleScreen:
                _currGameState = newState;
                OnStateChange?.Invoke(_currGameState);
                break;

            case GameState.GameRunning:
                _currGameState = newState;
                AudioListenerSwitch[] switches =FindObjectsOfType<AudioListenerSwitch>();
                foreach(AudioListenerSwitch currSwitch in switches){
                    currSwitch.InitSwitch();
                }
                OnStateChange?.Invoke(_currGameState);
                break;
            case GameState.Resetting:
                _currGameState = newState;
                OnStateChange?.Invoke(_currGameState);
                _currGameState = GameState.GameRunning; 
                OnStateChange?.Invoke(_currGameState);
                OnGameReset?.Invoke();
                break;
            case GameState.ResettingFromCheckPoint:
                _currGameState = newState;
                OnStateChange?.Invoke(_currGameState);

                _currGameState = GameState.GameRunning;
                OnStateChange?.Invoke(_currGameState);
                break;

            case GameState.LoadTitleScreen:
                _currGameState = newState;
                OnStateChange?.Invoke(_currGameState);
                break;

            case GameState.CreditScreen:
                _currGameState = newState;
                OnStateChange?.Invoke(_currGameState);
                break;

        }
    }

    //contains initilisation of game state manager
    private void SetUp()
    {
        //Initial state
        _currGameState = GameState.InitManagers;
        SpawnManagers();

        //call init on managers
        InitInitialManagers();

        runtimeData.isInitBoot = true;
        //call gameState
        OnStateChange?.Invoke(_currGameState);

      
      
    }


    private void SpawnManagers()
    {
        foreach(GameObject managerPrefab in managerPrefabs)
        {
            Instantiate(managerPrefab, Vector3.zero, Quaternion.identity);
        }
    
    }

    private void InitInitialManagers()
    {
        BaseManager[] managers = FindObjectsOfType<BaseManager>();

        foreach(BaseManager manager in managers)
        {
            manager.Init();
        }
    }
   
  
}
