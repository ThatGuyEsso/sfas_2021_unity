﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : BaseManager
{
    public Sound[] sounds;
    public SoundGroup[] soundGroups;
    public MusicManager _musicManagerPrefab;


    public static AudioManager _instance;
    public float pitchChange;
    override public void Init()
    {
        //Initialise Singleton _instance
        if (_instance == false)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        //Create sound class
        foreach (Sound s in sounds)
        {
            //Create audio source or respective sound
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.mixerGroup;
        }
        for(int i=0;i<soundGroups.Length;i++)
        {
            ////Create audio source for each sound group
            soundGroups[i].source = gameObject.AddComponent<AudioSource>();
            soundGroups[i].source.volume = soundGroups[i].volume;
            soundGroups[i].source.pitch = soundGroups[i].pitch;
            soundGroups[i].source.loop = soundGroups[i].loop;
            soundGroups[i].source.outputAudioMixerGroup = soundGroups[i].mixerGroup;
        }

        MusicManager newManager = Instantiate(_musicManagerPrefab, Vector3.zero, Quaternion.identity);
        newManager.Init();
        BindToGameStateManager();
    }

    public AudioSource PlayRandFromGroup(string groupName)
    {
        //Find Sound Group
        SoundGroup soundGroup = Array.Find(soundGroups, group => group.name == groupName);

        //load new clip into source

        if(soundGroup != null)
        {
            soundGroup.source.clip = soundGroup.GetRandClip();
            //Play new sound if it exists
            soundGroup.source.Play();
            return soundGroup.source;
        }
        else
        {
            Debug.Log("Group of name:" + groupName + " was not found");
            return null;
        }
    }
    public void PlayRandFromGroupAtRandPitch(string groupName)
    {
        //Find Sound Group
        SoundGroup soundGroup = Array.Find(soundGroups, group => group.name == groupName);

        //load new clip into source


        if (soundGroup != null)
        {
            soundGroup.source.clip = soundGroup.GetRandClip();
            soundGroup.source.pitch = GetRandomPitchOfSoundGroup(soundGroup);
            //Play new sound if it exists
            soundGroup.source.Play();
        }
        else
        {
            Debug.Log("Group of name:" + groupName + " was not found");
        }
    }

    //Play sound from sound name
    public Sound Play(string name)
    {
        Sound currentSound = Array.Find(sounds, sound => sound.name == name);
        if (currentSound != null)
        {
            currentSound.source.volume = currentSound.volume;
            currentSound.source.Play();
            return currentSound;

        }
        else
        {
            Debug.Log("Sound of name:" + name + " was not found");
            return null;
        }
    }

    public void BeginFadeOut(string name)
    {
        Sound currentSound = Array.Find(sounds, sound => sound.name == name);
        if (currentSound != null)
        {
            StartCoroutine(FadeOut(currentSound));
        }
        else
        {
            Debug.Log("Sound of name:" + name + " was not found");
        }
    }
    public Sound BeginFadeIn(string name)
    {
        Sound currentSound = Array.Find(sounds, sound => sound.name == name);
        if (currentSound != null)
        {
            StartCoroutine(FadeIn(currentSound));
            return currentSound;
        }
        else
        {
            Debug.Log("Sound of name:" + name + " was not found");
            return null;
        }
    }

    private IEnumerator FadeOut(Sound sound)
    {
        while(sound.source.volume > 0f)
        {
            //While sound is greater than 0f decrease it by 1%
            sound.source.volume -= sound.volume/ 100f;
            yield return null;
        }
        //done so stop volume and reset 
        sound.source.Stop();
        sound.source.volume = sound.volume;
     
    }
    private IEnumerator FadeIn(Sound sound)
    {
        sound.source.volume = 0;
        sound.source.Play();
        while (sound.source.volume < sound.volume)
        {
            //While sound is greater than 0f decrease it by 1%
            sound.source.volume += sound.volume / 100f;
            yield return null;
        }
        //done so stop volume and reset 
    
        sound.source.volume = sound.volume;

    }


    //Play sound of at random pitch 
    public void PlayAtRandomPitch(string name)
    {
        Sound currentSound = Array.Find(sounds, sound => sound.name == name);
        if (currentSound != null)
        {
            
            currentSound.source.volume = currentSound.volume;
            currentSound.source.pitch = GetRandomPitchOfSound(currentSound);
            currentSound.source.Play();
       

        }
        else
        {
            //Debug.Log("Sound of name:" + name + " was not found");
        }
    }

    //Stop a currently playing sound
    public void Stop(string name)
    {
        Sound currentSound = Array.Find(sounds, sound => sound.name == name);
        if (currentSound != null)
        {
            currentSound.source.Stop();
        }
        else
        {
            //Debug.Log("Sound of name:" + name + " was not found");
        }
    }

    public Sound GetSound(string name)
    {
        Sound currentSound = Array.Find(sounds, sound => sound.name == name);
        return currentSound;
    }
    public float GetRandomPitchOfSound(Sound sound)
    {
        return UnityEngine.Random.Range(sound.pitch - pitchChange, sound.pitch + pitchChange);
    }
    public float GetRandomPitchOfSoundGroup(SoundGroup group)
    {
        return UnityEngine.Random.Range(group.pitch - pitchChange, group.pitch + pitchChange);
    }
    public void BindToInitManager()
    {
        GameStateManager._instance.OnStateChange += EvaluateNewState;
    }

    protected override void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.InitManagers:
            
                break;
        }
    }
}
