﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionTracker : BaseManager
{
   [SerializeField] private List<string> _triggeredDecisions = new List<string>();
   public static DecisionTracker _instance;


    private void Awake()
    {
        Init();
    }
    public override void Init()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        //persist throughout levels
        DontDestroyOnLoad(gameObject);

        //Bind to game state manager
        BindToGameStateManager();
        DecisionManager._instance.OnDecisionMade += StoreDecision;
        isInitiated = true;
    }

    public void StoreDecision(string newDecision)
    {
        _triggeredDecisions.Add(newDecision);
    }


    protected override void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.Resetting:
                _triggeredDecisions.Clear();

                break;
            case GameState.InitGame:
                _triggeredDecisions.Clear();

                break;


        }
    }

    public List<string> GetTriggeredDecisions() { return _triggeredDecisions; }

    override protected void OnDestroy()
    {
        base.OnDestroy();
        if(isInitiated) DecisionManager._instance.OnDecisionMade -= StoreDecision;

    }
}
