﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : BaseManager
{

    public static MusicManager _instance;
    public static Sound _currentSong;
    private bool _isPlaying;
     [SerializeField] private List<string> _trackList;
    [SerializeField ]private float _minTimeBetwnSongs, _maxTimeBetwnSongs;
    private float _songDownTime;
    private int _currSongIndex=0;
    private bool _isHolding;
    public bool GetIsPlaying()
    {
        return _isPlaying =_currentSong.source.isPlaying;
    }

    public override void Init()
    {
        base.Init();
        //Initialise Singleton Instance
        if (_instance == false)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        _songDownTime = Random.Range(_minTimeBetwnSongs, _maxTimeBetwnSongs);
        BeginNewSongQueue();
    }

    private void Update()
    {
        if (!GetIsPlaying()&&!_isHolding)
        {
            if (_songDownTime <= 0)
            {
                _songDownTime = Random.Range(_minTimeBetwnSongs, _maxTimeBetwnSongs);
                BeginNewSongQueue();
            }
            else
            {
                _songDownTime -= Time.deltaTime;
            }
        }
    }
    protected override void EvaluateNewState(GameState newState)
    {
       
    }
    private void BeginNewSongQueue()
    {
        StartCoroutine(QueueNextTrack());
    }
    protected IEnumerator QueueNextTrack()
    {
        if(_currentSong != null)
        {
            while (_currentSong.source.isPlaying)
            {
                yield return null;
            }
        }
 


        _currentSong = AudioManager._instance.BeginFadeIn(_trackList[_currSongIndex]);
        _currSongIndex++;

        if (_currSongIndex > _trackList.Count - 1) _currSongIndex = 0;
        _songDownTime = Random.Range(_minTimeBetwnSongs, _maxTimeBetwnSongs);

      

    }
    public void BeginHoldSong(float time)
    {
        StopAllCoroutines();
        StartCoroutine(HoldSong(time));
    }
    public void StopMusic()
    {
        StopAllCoroutines();
        AudioManager._instance.Stop(_currentSong.name);
    }
    protected IEnumerator HoldSong(float time)
    {
        _isHolding = true;
        AudioManager._instance.Stop(_currentSong.name);

        yield return new WaitForSeconds(time);
        _currentSong = AudioManager._instance.BeginFadeIn(_trackList[_currSongIndex]);
        _isHolding = false;
    }
}
