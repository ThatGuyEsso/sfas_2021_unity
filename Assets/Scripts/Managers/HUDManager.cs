﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDManager : BaseManager
{
    public static HUDManager _instance;
    public TextPrompt prompt;

    private void Awake()
    {
        Init();
    }
    public override void Init()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        //Bind to game state manager
        BindToGameStateManager();

    }
}
