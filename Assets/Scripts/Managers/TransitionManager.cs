﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionManager : BaseManager
{
    public static TransitionManager _instance;

    //list of all scenes
    private List<AsyncOperation> _scenesLoading = new List<AsyncOperation>();

    public override void Init()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        //persist throughout levels
        DontDestroyOnLoad(gameObject);

        //Bind to game state manager
        BindToGameStateManager();
    }

    public void StartGame()
    {
        StartCoroutine(LoadGame());
    }
    public void GoToCredits()
    {
        StartCoroutine(LoadCredits());
    }

    //load in title screen at the start
    private IEnumerator LoadTitle()
    {
        LoadingScreen._instance.ToggleScreen(true);
      //get all currently loaded scenes
        Scene[] loadedScenes = GetAllActiveScenes();
    
        //add a unload operations
        foreach(Scene scene in loadedScenes)
        {
            if(scene.buildIndex != (int)SceneIndex.ComputerScene)
            _scenesLoading.Add(SceneManager.UnloadSceneAsync(scene));
        }

        //wait until every scene has unloaded
        for (int i = 0; i < _scenesLoading.Count; i++)
        {
            while (!_scenesLoading[i].isDone)
            {
                yield return null;
            }
        }
        //clear scens loading
        _scenesLoading.Clear();

        //begin loading title screen
         AsyncOperation worldScene = SceneManager.LoadSceneAsync((int)SceneIndex.TitleScreenScene, LoadSceneMode.Additive);

        while (!worldScene.isDone)
        {
            yield return null;
            
        }
        GameStateManager._instance.BeginNewState(GameState.TitleScreen);
        LoadingScreen._instance.ToggleScreen(false);
    }

    private IEnumerator LoadCredits()
    {
        LoadingScreen._instance.ToggleScreen(true);
        //get all currently loaded scenes
        Scene[] loadedScenes = GetAllActiveScenes();

        //add a unload operations
        foreach (Scene scene in loadedScenes)
        {
            if (scene.buildIndex != (int)SceneIndex.ComputerScene)
                _scenesLoading.Add(SceneManager.UnloadSceneAsync(scene));
        }

        //wait until every scene has unloaded
        for (int i = 0; i < _scenesLoading.Count; i++)
        {
            while (!_scenesLoading[i].isDone)
            {
                yield return null;
            }
        }
        //clear scens loading
        _scenesLoading.Clear();

        //begin loading title screen
        AsyncOperation worldScene = SceneManager.LoadSceneAsync((int)SceneIndex.CreditScene, LoadSceneMode.Additive);

        while (!worldScene.isDone)
        {
            yield return null;

        }
        GameStateManager._instance.BeginNewState(GameState.CreditScreen);
        LoadingScreen._instance.ToggleScreen(false);
    }
    private IEnumerator LoadGame()
    {
        //put up loading screen
        LoadingScreen._instance.ToggleScreen(true);

        //unload main menu
        AsyncOperation unloadLevel = SceneManager.UnloadSceneAsync((int)SceneIndex.TitleScreenScene);

        while (!unloadLevel.isDone)
        {
            yield return null;
        }

        //load computer screen scene
        AsyncOperation loadLevel = SceneManager.LoadSceneAsync((int)SceneIndex.ComputerScreenScene, LoadSceneMode.Additive);

        while (!loadLevel.isDone)
        {
            yield return null;
        }

        //load level scene
        loadLevel = SceneManager.LoadSceneAsync((int)SceneIndex.WorldScene, LoadSceneMode.Additive);
        while (!loadLevel.isDone)
        {
            yield return null;
        }

        GameStateManager._instance.BeginNewState(GameState.InitGame);
        LoadingScreen._instance.ToggleScreen(false);//loading complete so hide loading screen
    }

    

    protected override void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.InitManagers:
                StartCoroutine(LoadTitle());
                break;

            case GameState.LoadTitleScreen:
                StartCoroutine(LoadTitle());
                break;
        }
    }

    private Scene[] GetAllActiveScenes()
    {
        int countLoaded = SceneManager.sceneCount;
        Scene[] loadedScenes = new Scene[countLoaded];

        for (int i = 0; i < countLoaded; i++)
        {
            loadedScenes[i]=(SceneManager.GetSceneAt(i));
        }

        return loadedScenes;
    }
}
