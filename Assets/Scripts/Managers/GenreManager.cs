﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Genres
{
    TextAdventure,//when in the computer terminal
    FirstPersonAdeventure, //when first person adventure game
    ChangingGenre //When transitioning between genres
};

public class GenreManager : BaseManager
{
    public static GenreManager _instance;
    public static Genres _currGenre;

    //for genre transitions
    public event GenreChangeDelegate OnGenreChange;
    public delegate void GenreChangeDelegate(Genres newGenre);

    //for direct/instant genre changes
    public event NewGenreDelegate OnDirectChange;
    public delegate void NewGenreDelegate(Genres newGenre);

    //for displaying relevant beats
    public event NewStoryBeatDelegate OnNewBeat;
    public delegate void NewStoryBeatDelegate(int ID);
    public override void Init()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        //persist throughout levels
        DontDestroyOnLoad(gameObject);

        //Bind to game state manager
        BindToGameStateManager();

        //set initial genre to be textbased
        _currGenre = Genres.TextAdventure;
    }

    public void ChangeGenre(Genres newGenre)
    {
        _currGenre = Genres.ChangingGenre;
        OnGenreChange?.Invoke(newGenre);
        switch (newGenre)
        {
            case Genres.FirstPersonAdeventure:
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                break;
            case Genres.TextAdventure:
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = false;
                break;
        }
    }
 
    public void BeginNewStoryBeat(int ID)
    {
        OnNewBeat?.Invoke(ID);
    }

    protected override void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.InitGame:
                _currGenre = Genres.TextAdventure;
                break;
        }
    }
    //Only want others to be able to call the new genre if it is in a transitional stage
    public void SetCurrentGenre(Genres newGenre)
    {
        if (IsChangingGenre())
        {
            _currGenre = newGenre;
        }
    }
    //Functions to query current game genre
    public static bool IsTextAdventure() { return _currGenre == Genres.TextAdventure; }
    public static bool IsFirstPersonAdventure() { return _currGenre == Genres.FirstPersonAdeventure; }
    public static bool IsChangingGenre() { return _currGenre == Genres.ChangingGenre; }

    //should only be used in specific circumstances, where you don't want a visual transition to occur
    public void DirectlySetGenre(Genres newGenre)
    {
        _currGenre = newGenre;
        OnDirectChange?.Invoke(newGenre);
    }
}
