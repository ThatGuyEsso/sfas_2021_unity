﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionManager : BaseManager
{
    //static instance
    public static DecisionManager _instance;
    [SerializeField] private DecisionData _decisionData;


    public event NewDecisionDelegate OnDecisionMade;
    public delegate void NewDecisionDelegate(string decision);
    public static int nextBeat;

  


    //Initialisation 
    public override void Init()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        //Bind to game state manager
        BindToGameStateManager();
        //persist throughout levels

        _decisionData.GenerateConsequenceID();
        DontDestroyOnLoad(gameObject);
    }


    public void ResetDecisions()
    {
        _decisionData.ResetAllDecisions();
    }

    protected override void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.InitGame:
            {
                ResetDecisions();
                break;
            }
        }
    }

    //take beat choice and review decision information within it
    public int EvaluateChoice(ChoiceData choice)
    {
        switch (choice.TriggerType)
        {
            case ChoiceTriggerType.Null:
                //does choice need validation
                if (choice.NeedsValidation)
                {
                    //Store and get the dependent consequence
                    Consequence dependentConsequence;
            
                    //if consequence exist
                    if ((dependentConsequence = _decisionData.GetConsequenceByName(choice.DependentConsequence))!= null)
                    {
                        //if it has been triggered then the choice is valid
                        if (dependentConsequence.GetIsTriggered())
                        {
                            //is ID valid
                            if(choice.ValidID > 1)
                            {
                                //trigger associated consequence if it exists
                                Consequence validConsequence;
                                if ((validConsequence = _decisionData.GetConsequenceByName(choice.ValidConsequenceName))!= null){

                                    validConsequence.TriggerConsequence();
                                    //Invoke delegates who are interested in new consequences being triggered
                                    OnDecisionMade?.Invoke(choice.ValidConsequenceName);
                                }
                                return choice.ValidID;
                            }

                        }//if not triggered is is invalid return the invalid ID
                        else
                        {
                            //is ID valid
                            if (choice.InvalidID > 1)
                            {
                                //trigger associated consequence if it exists
                                Consequence invalidConsequence;
                                if ((invalidConsequence = _decisionData.GetConsequenceByName(choice.InvalidConsequenceName)) != null)
                                {
                                    //Invoke delegates who are interested in new consequences being triggered
                                    invalidConsequence.TriggerConsequence();
                                    OnDecisionMade?.Invoke(choice.InvalidConsequenceName);
                                }
           
                                return choice.InvalidID;
                            }
                        }
                    }
                }
                else
                {
                    if (choice.NextID > 1)
                    {
                        Consequence consequence;
                        //Find consequence by name 
                        if ((consequence = _decisionData.GetConsequenceByName(choice.ConsequenceName)) != null)
                        {
                            //if it exist 

                            //Invoke delegates who are interested in new consequences being triggered
                            OnDecisionMade?.Invoke(choice.ConsequenceName);
                            consequence.TriggerConsequence(); //Trigger consequence
                            Debug.Log(" consequence:" + consequence.GetConsequenceName());
                        }

                        //store reference to next beat so game story knows where to resume from
                        return nextBeat = choice.NextID;
                    }
                    else
                    {

                        //log id value
                        Debug.LogError("Invalid Beat ID: " + choice.NextID);
                        Application.Quit();//If next choice is less than one an error has and game can't be completed so quit


                    }

                }
                break;
              
            case ChoiceTriggerType.RestartGame:
                GameStateManager._instance.BeginNewState(GameState.Resetting);
                _decisionData.ResetAllDecisions();
                //first beat
                return 1;

            case ChoiceTriggerType.CheckPoint:
                _decisionData.ResetAllDecisions();
                GameStateManager._instance.BeginNewState(GameState.ResettingFromCheckPoint);
                //Idle beat
                return 91;

            case ChoiceTriggerType.MainMenu:
                GameStateManager._instance.BeginNewState(GameState.LoadTitleScreen);
                //AudioManager._instance.Stop("DeletingtSFX");
                //Idle beat
                return 90;





        }
        //íf nothing is returned return 0 which is invalid;
        return 0;
    }

    //Allows consequence to found with out a reference to the decision data object
    public Consequence GetConsequenceByName(string consequence)
    {
        return _decisionData.GetConsequenceByName(consequence);
    }
}
