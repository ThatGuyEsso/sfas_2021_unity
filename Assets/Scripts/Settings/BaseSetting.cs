﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSetting : MonoBehaviour
{
    [SerializeField] protected float _maxValue;
    [SerializeField] protected float _minValue;
    [SerializeField] protected float _currentValue;
    [SerializeField] protected float _percentChange;
    [SerializeField] protected BlockSlider _slider;

    public virtual void SetUp()
    {
        _slider.SetUpSlider(_maxValue,_minValue,_currentValue);
    }

    public virtual void Display()
    {
        _slider.DisplaySlider();
    }

    public virtual void IncrementValue()
    {
        _currentValue += _maxValue * _percentChange;
        if (_currentValue > _maxValue) _currentValue = _maxValue;
        _slider.UpdateSlider(_currentValue);
    }

    public virtual void DecrementValue()
    {
        _currentValue -= _maxValue * _percentChange;
        if (_currentValue < _minValue) _currentValue = _minValue;
        _slider.UpdateSlider(_currentValue);
    }
}
