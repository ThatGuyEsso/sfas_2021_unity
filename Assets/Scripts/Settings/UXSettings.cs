﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UXSettings : BaseSetting
{
    [SerializeField] private UserSettings _settings;


    public override void SetUp()
    {
        base.SetUp();
        _settings._mouseSensitivity = _currentValue;

    }
    public override void IncrementValue()
    {

        base.IncrementValue();
        _settings._mouseSensitivity = _currentValue;
    }

    public override void DecrementValue()
    {
        base.DecrementValue();

        _settings._mouseSensitivity = _currentValue;
    }

}
