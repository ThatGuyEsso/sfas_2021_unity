﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingFunction :BaseSetting
{

    public override void Display()
    {
        
        //do nothing
    }

    public override void SetUp()
    {
        //do nothing
    }
    public override void IncrementValue()
    {
        GameStateManager._instance.BeginNewState(GameState.LoadTitleScreen);
        SettingsMenu._instance.ToggleSettings();
    }

    public override void DecrementValue()
    {
        Application.Quit();
    }
}
