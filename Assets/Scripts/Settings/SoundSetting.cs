﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class SoundSetting : BaseSetting
{
    [SerializeField] private AudioMixerGroup _mixerGroup;

    [SerializeField] private float currentDB;

    public override void SetUp()
    {
        base.SetUp();

        currentDB = _currentValue - _maxValue;
        _mixerGroup.audioMixer.SetFloat("volume", currentDB);
    }
    public override void IncrementValue()
    {
      
        base.IncrementValue();
        currentDB =   _currentValue - _maxValue;
        _mixerGroup.audioMixer.SetFloat("volume", currentDB);
    }

    public override void DecrementValue()
    {
        base.DecrementValue();
   
        currentDB -= _maxValue * _percentChange;
        _slider.UpdateSlider(_currentValue);
        //float floatToDB = (_maxValue -_currentValue) - _currentValue;
        _mixerGroup.audioMixer.SetFloat("volume", currentDB);
    }

  
}
