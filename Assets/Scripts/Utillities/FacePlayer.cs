﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacePlayer : MonoBehaviour
{
    [SerializeField] private List<string> _facePlayerTriggers;


    [SerializeField] private bool _shouldFacePlayer = false;
    private Transform playerTrans;


    public void Awake()
    {
        foreach (string consequence in _facePlayerTriggers)
        {
            DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered += BeginFacingPlayer;
        }

        playerTrans = FindObjectOfType<FirstPersonBehaviour>().transform;

    }


    public void Update()
    {
        if(GenreManager.IsFirstPersonAdventure() && _shouldFacePlayer)
        {
            FacePlayerCharacter();
        }
    }

    public void FacePlayerCharacter()
    {

        transform.LookAt(playerTrans.position);
    }
    private void BeginFacingPlayer()
    {
        _shouldFacePlayer = true;
    }
}
