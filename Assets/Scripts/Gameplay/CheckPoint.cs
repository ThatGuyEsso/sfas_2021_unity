﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    [SerializeField] List<string> _storedDecisions= new List<string>();
    [SerializeField ]List<string> _activationConsequences = new List<string>();
    [SerializeField] private List<TerminalTransition> _terminalTransitions;
    [SerializeField] private List<BaseNPC> _npcs;
    [SerializeField] private List<BaseInteractableObject> _objects;
    [SerializeField] private List<BaseInteractableObject> _objectsToEnable;

    [SerializeField] private RouteGates _gateToOpen;
    private bool isActive;

    public event ActivateDelegate OnActivateCheckPoint;
    public delegate void ActivateDelegate(CheckPoint newCheckPoint);

    private void Awake()
    {
        foreach(string decision in _activationConsequences)
        {
            DecisionManager._instance.GetConsequenceByName(decision).OnConsequenceTriggered += Activate;
        }

    }

    public void Activate()
    {
        //Wait some time to ensure all decisions are updated before stored
        Invoke("StoreCheckPointData", 2.0f);
    }

    private void StoreCheckPointData()
    {
        if (DecisionTracker._instance.GetTriggeredDecisions().Count > 0)
        {
            List<string> decisions = DecisionTracker._instance.GetTriggeredDecisions();
            foreach (string decision in decisions)
            {
                _storedDecisions.Add(decision);
            }
            isActive = true;
            OnActivateCheckPoint?.Invoke(this);
        }

    }
    public void Deactivate()
    {
        isActive = false;
    }

    public void Init()
    {
        _storedDecisions.Clear();
        isActive = false;
        foreach (TerminalTransition terminal in _terminalTransitions)
        {
            terminal.ResetTerminal();
        }
    }


    public void RestoreDecisions()
    {
        foreach (string decision in _storedDecisions)
        {
            //Do not trigger consequence but update it to show it has been.
            DecisionManager._instance.GetConsequenceByName(decision).SetIsTriggered(true);
        }
        foreach(TerminalTransition terminal in _terminalTransitions)
        {
            terminal.ResetTerminal();
        }
        foreach (BaseNPC npc in _npcs)
        {
            npc.ResetNPC();
        }
        foreach(BaseInteractableObject obj in _objects)
        {
            obj.ResetObject();
        }
        foreach (BaseInteractableObject obj in _objectsToEnable)
        {
            obj.BecomeInteractable();
        }
        if (_gateToOpen != false)
        {
            _gateToOpen.BeginToOpen();
        }
   
    }

    public bool IsActive { get { return isActive; } }
}
