﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonBehaviour : MonoBehaviour
{
    private Rigidbody _rb;
    [SerializeField]
    private GameObject povCam;



    [SerializeField] private float _walkSFXplayRate;
    private float _currSFXwaitTime;
    //movement
    [SerializeField]
    private float _maxSpeed; 
    private Vector3 moveDir;
    [Header("Camera Settings")]
    [SerializeField] private UserSettings settings;

    private float _xRotation, _yRot;
    [SerializeField] private ViewBob _viewBobCam;
    [SerializeField] private float _viewBobFreq, _viewBobHorizAmp, _viewBobVertAmp, _bobSmoothing;
    //acceleration variables
    [SerializeField]
    private float _timeToMax, _timeToZero;
    

    //Controller state
    private bool isMoving;


    private void Awake()
    {
        Init();
    }


    private void Init()
    {
        //cache character components
        _rb = gameObject.GetComponent<Rigidbody>();
        _currSFXwaitTime = 0;
    }


    private void Update()
    {
        //only update input if it's a first person adventure
        if (GenreManager.IsFirstPersonAdventure())
        {
            UpdateMovementInput();
            PlayWalkingSoundeffect();
           

        }
        //only update mouse position while game is transitioning or in first person adventure
        //This makes the camera movement smooth, rather suddenly snapping to the new position when game mode updates.
        if (!GenreManager.IsTextAdventure())
        {
            UpdateMouseLook();
        }
        

    }
    private void FixedUpdate()
    {
        HandleMovement();

    }

    private void HandleMovement()
    {
        if (GenreManager.IsFirstPersonAdventure())
        {     //If on the move
            if (isMoving)
            {
                //accelerate character to max speed
                Move(_maxSpeed, moveDir);
                _viewBobCam.BeginViewBob(_viewBobFreq, _viewBobHorizAmp, _viewBobVertAmp, _bobSmoothing);

            }
            else
            {
                Stop();
                _viewBobCam.EndViewBob();
            }
        }
        else
        {
            Stop();
            _viewBobCam.EndViewBob();
        }
    }
    public void UpdateMovementInput()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if(x != 0f || z != 0f)
        {
            isMoving = true;
            moveDir = (x * transform.right + z * transform.forward).normalized;
        }
        else
        {
            isMoving = false;
        }
    }

    private void UpdateMouseLook()
    {
        float mousePosX = Input.GetAxis("Mouse X") * settings._mouseSensitivity * Time.deltaTime;//Get mouse position independent of framew rate
        float mousePosY = Input.GetAxis("Mouse Y") * settings._mouseSensitivity * Time.deltaTime;//Get mouse position independent of framew rate
        _xRotation -= mousePosY;
        _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);

        Vector3 rot = povCam.transform.localRotation.eulerAngles;
        _yRot = rot.y + mousePosX;

        povCam.transform.localRotation = Quaternion.Euler(_xRotation, _yRot, 0f);
        transform.localRotation = Quaternion.Euler(0f, _yRot, 0f);
    }

    //accelerate character in XZ plane dependant on input 
    private void Move(float maxSpeed, Vector3 dir)
    {


        //set speed in XZ plane but keep applying gravity
        _rb.velocity = new Vector3(dir.x * _maxSpeed * Time.deltaTime, _rb.velocity.y, dir.z * _maxSpeed * Time.deltaTime);
    }
    private void Stop()
    {
   
        //Stop movement in XZ plane but keep applying gravity
        _rb.velocity =  new Vector3(0f, _rb.velocity.y, 0.0f); ;
    }


    private void PlayWalkingSoundeffect()
    {
        //when moving
        if (isMoving)
        {
            if(_currSFXwaitTime <= 0)
            {
                AudioManager._instance.PlayRandFromGroup("DirstStepSFX");
                _currSFXwaitTime = _walkSFXplayRate;
            }
            else
            {
                _currSFXwaitTime -= Time.deltaTime;
            }
        }
    }
}
