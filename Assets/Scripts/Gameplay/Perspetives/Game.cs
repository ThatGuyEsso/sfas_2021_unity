﻿using System.Collections;
using UnityEngine;

public class Game : MonoBehaviour
{
    //Curren data used in story
    [SerializeField] private StoryData _data;
    public static Game _instance;
    private bool isDisplayDone;

    private TextDisplay _output;    //text component
    private BeatData _currentBeat; //Data of the individual story beat
    private WaitForSeconds _wait; //delay
    [SerializeField] private GameObject _continuePrompt;
    private void Awake()
    {
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
        BindToGameStateManager();
        _continuePrompt.SetActive(false);
        //work around to stop null references until issue is tracked down



    }
    private void Init()
    {
        _output = GetComponentInChildren<TextDisplay>();
        _currentBeat = null;
        _wait = new WaitForSeconds(0.01f);
        DisplayBeat(1);

        //Set beat evaluation as a delegate to when text finished displaying
        _output.OnTextDisplayed += EvaluateBeat;
    }
    private void Update()
    {
        //only update text display if in the textadventure genre
        if (GenreManager.IsTextAdventure() && GameStateManager._currGameState == GameState.GameRunning)
        {

            if(_output.IsIdle)
            {
                if (_currentBeat == null)
                {
                    DisplayBeat(1);
                }
                else
                {
                    UpdateInput();
                }
            }
        }

       
    }

    private void UpdateInput()
    {
        
        //
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    if(_currentBeat != null)
        //    {
        //        if (_currentBeat.ID == 1)
        //        {
        //            Application.Quit();
        //        }
        //        else
        //        {
        //            DisplayBeat(1);
        //        }
        //    }
        //}
       
        KeyCode alpha = KeyCode.Alpha1;
        KeyCode keypad = KeyCode.Keypad1;

        for (int count = 0; count < _currentBeat.Decision.Count; ++count)
        {
            if (alpha <= KeyCode.Alpha9 && keypad <= KeyCode.Keypad9)
            {
                if (Input.GetKeyDown(alpha) || Input.GetKeyDown(keypad))
                {
                    AudioManager._instance.PlayAtRandomPitch("MenuSelectSFX");
                    ChoiceData choice = _currentBeat.Decision[count];

                    //Let decision manager evaluate next choice, and determine next beat to present
                    DisplayBeat(DecisionManager._instance.EvaluateChoice(choice));
                   
                    break;
                }
            }

            ++alpha;
            ++keypad;
        }
        
    }

    private void DisplayBeat(int id)
    {
        BeatData data = _data.GetBeatById(id);
        if(id != 90) StartCoroutine(DoDisplay(data));

        _currentBeat = data;
       
    }

    private IEnumerator DoDisplay(BeatData data)
    {
        _output.Clear();

        while (_output.IsBusy)
        {
            yield return null;
        }
        AudioManager._instance.PlayAtRandomPitch("TypingSFX");
        _output.Display(data.DisplayText);
        while (_output.IsBusy)
        {
            //While printing wait and see if user wants to skip
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _output.SkipDisplay(data.DisplayText);
            }
            yield return null;
        }
        
        for (int count = 0; count < data.Decision.Count; ++count)
        {
            ChoiceData choice = data.Decision[count];
            _output.Display(string.Format("{0}: {1}", (count + 1), choice.DisplayText));

            while (_output.IsBusy)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    //incase you want to skip choices being printed
                    _output.SkipChoice(data.DisplayText, data.Decision);
                    break;
                }
                yield return null;
            }
        }
        AudioManager._instance.Stop("TypingSFX");
        if (data.Decision.Count > 0)
        {
            _output.ShowWaitingForInput();
        }
    }


    private void EvaluateBeat()
    {
        //Don't evaluate if game isn't running
      if(GameStateManager._currGameState == GameState.GameRunning)
      {
            //Pass beat to decision manager to mange values
            if (_currentBeat.IsTransitionTrigger())
            {
                //beat is a transtion trigger so begin following transition
                StartCoroutine(HoldTransition());
            }
      }
        
    }
    private IEnumerator HoldTransition() {
     
        bool moveOn =false;
        //wait till not printing
        while (_output.IsBusy)
        {
         
            yield return null;
        }
        _continuePrompt.SetActive(true);
        while (!moveOn)
        {
            
            //on key prompt break out
            if(moveOn = Input.GetKeyDown(KeyCode.E))
            {
                AudioManager._instance.PlayAtRandomPitch("MenuSelectSFX");
            }
            yield return null;
        }
        //disable prompt
        _continuePrompt.SetActive(false);
        //continue transition
        StartCoroutine(FollowTransition());
    }
    private IEnumerator FollowTransition()
    {
        //wait some time beofre starting 
        yield return new WaitForSeconds(0.5f);
        isDisplayDone = false;

        //Subscribe to when display is done clearing
        _output.OnTextCleared += ResetIsDisplayDone;

        //begin clear
        _output.Clear();
        //wait until display is done clearing
        while (!isDisplayDone)
        {
            Debug.Log("Waiting for output");
            yield return null;
        }
        Debug.Log("Done waiting");

        //Done listining so unsubscrube to when display is done clearing
        _output.OnTextCleared -= ResetIsDisplayDone;

        
        GenreManager._instance.ChangeGenre(Genres.FirstPersonAdeventure);

    }

    private void ResetIsDisplayDone()
    {
        isDisplayDone = true;
    }


    public void BindToGameStateManager()
    {
        GameStateManager._instance.OnStateChange += EvaluateNewState;
        GenreManager._instance.OnNewBeat += DisplayBeat;
    }

    protected void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.InitGame:
                Init();
                break;

            case GameState.TitleScreen:
                //_output.ResetDisplay();
                break;
        }
    }

    private void OnDestroy()
    {
        _output.OnTextDisplayed -= EvaluateBeat;
        GameStateManager._instance.OnStateChange -= EvaluateNewState;
        GenreManager._instance.OnNewBeat -= DisplayBeat;
        _output.OnTextCleared -= ResetIsDisplayDone;
    }
}

