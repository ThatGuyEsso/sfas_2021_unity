﻿using System.Collections;
using UnityEngine;
using TMPro;
using System.Collections.Generic;

public class TextDisplay : MonoBehaviour
{
    public enum State { Initialising, Idle, Busy }
    
    private TMP_Text _displayText; //text show to player
    private string _displayString; //string of characters to display
    private WaitForSeconds _shortWait; //short wait period
    private WaitForSeconds _longWait; // long wait period
    private State _state = State.Initialising;

    //Delegates
    public event DoneDisplayingTextDelegate OnTextDisplayed; 
    public event DoneClearingTextDelegate OnTextCleared;
    public delegate void DoneDisplayingTextDelegate();
    public delegate void DoneClearingTextDelegate();
    public bool IsIdle { get { return _state == State.Idle; } } //Is true if game is idle
    public bool IsBusy { get { return _state != State.Idle; } } //is true if game is not idle

    private void Awake()
    {
        //Text component
        _displayText = GetComponent<TMP_Text>();

        //Wait times
        _shortWait = new WaitForSeconds(0.04f);
        _longWait = new WaitForSeconds(1f);

        //Initial box to empty
        _displayText.text = string.Empty;

        //Start Idle
        _state = State.Idle;
    }

    //write text character one by one
    private IEnumerator DoShowText(string text)
    {
        //start at firt string index
        int currentLetter = 0;

        //cast string to print to char array
        char[] charArray = text.ToCharArray();

        //Print until end of line has been reached
        while (currentLetter < charArray.Length)
        {
            //append text with crrent character
            _displayText.text += charArray[currentLetter++];
            yield return _shortWait;//wait time between character prints
        }

        //start new line when end of line has been reached
        _displayText.text += "\n";

        //current string displayed is the string
        _displayString = _displayText.text;


        //done writing so new state is idle.
        _state = State.Idle;

        //invoke text finished displaying
        OnTextDisplayed?.Invoke();

   
    }

    public void SkipDisplay(string text)
    {
        //stop displaying
        StopAllCoroutines();
        _displayText.text = text;
        //start new line when end of line has been reached
        _displayText.text += "\n";

        //current string displayed is the string
        _displayString = _displayText.text;


        //done writing so new state is idle.
        _state = State.Idle;

        //invoke text finished displaying
        OnTextDisplayed?.Invoke();

    }
    public void SkipChoice(string body, List<ChoiceData> questions)
    {
        StopAllCoroutines();
        //the display beat text
        _displayText.text = body;
        _displayText.text += "\n";
        //loop through all choices and append them to the end of the display
        for (int i = 0; i < questions.Count; i++)
        {
            _displayText.text += (string.Format("{0}: {1}", (i + 1), questions[i].DisplayText));
            //start new line when question has been added
            _displayText.text += "\n";

        }



        //current string displayed is the string
        _displayString = _displayText.text;


        //done writing so new state is idle.
        _state = State.Idle;

        //invoke text finished displaying
        OnTextDisplayed?.Invoke();

    }
    private IEnumerator DoAwaitingInput()
    {
        bool on = true;

        while (enabled)
        {
            //string formating
            _displayText.text = string.Format( "{0}> {1}", _displayString, ( on ? "|" : " " ));
            on = !on;

            //wait long
            yield return _longWait;

        }
    }

    private IEnumerator DoClearText()
    {
        //Start and index 0
        int currentLetter = 0;

        //cast display text to clear to char array
        char[] charArray = _displayText.text.ToCharArray();

        AudioManager._instance.PlayAtRandomPitch("DeletingtSFX");
        //Go to end of string
        while (currentLetter < charArray.Length)
        {
            
            
            //if letter is not at begining, or at a new line
            if (currentLetter > 0 && charArray[currentLetter - 1] != '\n')
            {
                //Clear letter previous letter
                charArray[currentLetter - 1] = ' ';
            }

            //if not new line 
            if (charArray[currentLetter] != '\n')
            {
                //clear currebt ketter
                charArray[currentLetter] = '_';
            }

            //Update current text display
            _displayText.text = charArray.ArrayToString();
            //move to next letters
            ++currentLetter;
            Debug.Log("Deleting");
   
            yield return null;
        }
        Debug.Log(" Done Deleting");
        AudioManager._instance.Stop("DeletingtSFX");
        //Done so clear display string
        _displayString = string.Empty;

        //Clear text display
        _displayText.text = _displayString;

        //done clearing so state is idle
        _state = State.Idle;

        //invoke text finished clearing
        OnTextCleared?.Invoke();
    }

    //display function call
    public void Display(string text)
    {
        //if not currently active
        if (_state == State.Idle)
        {
            //stop all coroutine (To stop overlap))
            StopAllCoroutines();
            _state = State.Busy;//currently busy

            //Begin to show THIS text
            StartCoroutine(DoShowText(text));
        }
    }

   
    public void ShowWaitingForInput()
    {
        if (_state == State.Idle)
        {
            StopAllCoroutines();
            StartCoroutine(DoAwaitingInput());
        }
    }

    //Function call to begin clearing text
    public void Clear()
    {
        if (_state == State.Idle)//only clear if not currently intiation or writing
        {
            StopAllCoroutines();
            //it is now clearing so busy
            _state = State.Busy;
            //begin clear text
            StartCoroutine(DoClearText());
        }
    }


    public void ResetDisplay()
    {
        StopAllCoroutines();
        AudioManager._instance.Stop("DeletingtSFX");
        //Done so clear display string
        _displayString = string.Empty;

        //Clear text display
        _displayText.text = _displayString;

        //done clearing so state is idle
        _state = State.Idle;

        //invoke text finished clearing
        OnTextCleared?.Invoke();
    }
}
