﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouteGates : MonoBehaviour
{
    [SerializeField] private List<string> _triggerConsequences; // consequence bound to
    [SerializeField] private float _hideAmount = 4f; //units to move wall

    [SerializeField] private bool _isConsequenceDependent; // whether it waits for a consequence to happen before opening
    [SerializeField] private bool _isOpen = false;
    [SerializeField] private bool triggerScreenShake = true;
    [SerializeField] private GameObject _doorVFX; //vfx played while wall either disappers or appears
    [SerializeField] private Transform _vfxPoint;
    [SerializeField] private CheckPoint _checkPoint;
    private bool _isActive;

    [SerializeField] private float _moveRate,_closeRate, _moveAmount;

    //Triggers  to detect when player enters and leaves
    private BlockadeTrigger _exitTrigger;
    private BlockadeTrigger _enterTrigger;





    public void Awake()
    {
        Init();
    }


    public void Init()
    {
     
        //if the opening of door is dependent on a consequence
        if (_isConsequenceDependent)
        {

            foreach(string consequence in _triggerConsequences)
            {
                DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered += BeginToOpen;
            }
            //bind to that unique instance
          
            _exitTrigger = transform.Find("Exit Trigger").GetComponent<BlockadeTrigger>();
            if (_exitTrigger != false)
            {
                _exitTrigger.OnTrigger += BeginToClose;


            }
           
        }
        else
        {
            //Cache trigger components
            _enterTrigger = transform.Find("Open Trigger").GetComponent<BlockadeTrigger>(); 
            _exitTrigger = transform.Find("Exit Trigger").GetComponent<BlockadeTrigger>();
            if (_exitTrigger != false)
            {
                _exitTrigger.OnTrigger += BeginToClose;

            }
            if(_enterTrigger!= false)
            {
                _enterTrigger.OnTrigger += BeginToOpen;
            }
           
        }
 
    }


    public void BeginToOpen()
    {
        if (!_isActive)
        {
            StopAllCoroutines();
            StartCoroutine(OpenGateOverTime());
          
        }
    }


    private void BeginToClose()
    {
        if (!_isActive)
        {
            if (_checkPoint != false) _checkPoint.Activate();
            StopAllCoroutines();
            StartCoroutine(CloseGateOverTime());
           
        }
    }

    private IEnumerator OpenGateOverTime()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait t
        }

        if (!_isOpen)
        {
            LoopingParticleCleanUp vfx = Instantiate(_doorVFX, _vfxPoint.transform.position, Quaternion.identity).GetComponent< LoopingParticleCleanUp>();
            if (triggerScreenShake)
            {
                AudioManager._instance.PlayAtRandomPitch("RumbleSFX");
                CamShake._instance.StartScreenShake(0.1f, 0.01f);
            }
            _isActive = true;
            float currY = transform.position.y;
            float endY = transform.position.y - _hideAmount;
            while(currY > endY)
            {
                currY -= _moveAmount;
                transform.position = new Vector3(transform.position.x, currY, transform.position.z);
                yield return new WaitForSeconds(_moveRate);
            }
            _isOpen = true;
            _isActive = false;
            if (triggerScreenShake)
            {
                CamShake._instance.EndScreenShake(0.01f);
                AudioManager._instance.BeginFadeOut("RumbleSFX");
            }
            vfx.CleanUpVFX();
        }
       
    }
    private IEnumerator CloseGateOverTime()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait t
        }

        if (_isOpen)
        {
           
            Debug.Log("close gate");
            _isActive = true;
            float currY = transform.position.y;
            float endY = transform.position.y + _hideAmount;
            while (currY < endY)
            {
                currY += _moveAmount;
                transform.position = new Vector3(transform.position.x, currY, transform.position.z);
                yield return new WaitForSeconds(_closeRate);
            }
            _isOpen = false;
            _isActive = false;
          
        }

    }

    private void OnDestroy()
    {
        if (DecisionManager._instance != false)
        {
            foreach (string consequence in _triggerConsequences)
            {
                DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered -= BeginToOpen;
            }
        }
  

        if (_exitTrigger != false)
        {
            _exitTrigger.OnTrigger -= BeginToClose;


        }
        if (_enterTrigger != false)
        {
            _enterTrigger.OnTrigger -= BeginToOpen;
        }
    }
}
