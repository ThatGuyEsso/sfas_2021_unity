﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseNPC : MonoBehaviour
{

    [SerializeField] protected int _defaultBeatID;
    [SerializeField] protected string _consequenceTrigger;//trigger that causes npc to point to a new beat
    [SerializeField] protected string _promptText;
    [SerializeField] protected GameObject _disappearVFX;
    protected bool isInitiated = false;
    protected int _nextBeatID;
    protected bool _inRange = false;//player is in trigger volume
    protected bool _isActivated = false;//haven't already been used
    protected virtual void Awake()
    {
        _nextBeatID = _defaultBeatID;
        GameStateManager._instance.OnGameReset += ResetNPC;
    }

    public virtual void ResetNPC()
    {
        _isActivated = false;
        _inRange = false;
    }
    virtual protected void Update()
    {
        if (GenreManager.IsFirstPersonAdventure()&&_inRange)
        {
            if (_inRange && !_isActivated){
                UpdateInput();
            }
        }
    }

    protected virtual void BeginTransition()
    {
        GenreManager._instance.ChangeGenre(Genres.TextAdventure);
        GenreManager._instance.BeginNewStoryBeat(_nextBeatID);
        _isActivated = true;
    }



    protected virtual void UpdateInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //trigger
          
            HUDManager._instance.prompt.HideText();
            BeginTransition();
        
         
        }
    }

    protected virtual void OnDestroy()
    {
        if (isInitiated)
        {
         
            GameStateManager._instance.OnGameReset -= ResetNPC;
        }
      
    }

    virtual protected void ActivateComponents()
    {

    }

    virtual protected void DeactivateComponents()
    {

    }
}
