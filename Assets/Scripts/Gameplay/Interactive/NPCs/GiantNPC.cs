﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantNPC : BaseNPC
{
    private Animator anim;
    [SerializeField] private Animator giantAnim;
    [SerializeField] private string _defeatBothCon;
    [SerializeField] private string _defeatBigCon;
    [SerializeField] private string _escapeCon;
    [SerializeField] private Transform _escapePoint;

    [SerializeField] private float _timeBeforeExecution;
    [SerializeField] private GameObject _superGiant,_smallGiant;
    private Transform _playerTrans;
    protected override void Awake()
    {
        base.Awake();
        anim = gameObject.GetComponent<Animator>();
        SetUp();


    }
    void SetUp()
    {

        isInitiated = true;
        DeactivateComponents();
        DecisionManager._instance.GetConsequenceByName(_defeatBothCon).OnConsequenceTriggered += BeginBothDefeated;
        DecisionManager._instance.GetConsequenceByName(_defeatBigCon).OnConsequenceTriggered += BeginBigGiantDefeated;
        DecisionManager._instance.GetConsequenceByName(_escapeCon).OnConsequenceTriggered += PlayerPassed;
        //activate when player goes down this path
        DecisionManager._instance.GetConsequenceByName(_consequenceTrigger).OnConsequenceTriggered += ActivateComponents;
        if (giantAnim != false) giantAnim.enabled = false;

    }
    //On enter and exit event checks if it is player
    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            _playerTrans = other.transform;
            _inRange = true;
            HUDManager._instance.prompt.DisplayText(_promptText);
        }
    }
    protected void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            _playerTrans = null;
            HUDManager._instance.prompt.HideText();
            _inRange = false;
        }
    }

    private void BeginBigGiantDefeated()
    {
        StopAllCoroutines();
        StartCoroutine(BigGiantDefeated());


    }
    private IEnumerator BigGiantDefeated()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait till back in game
        }
        yield return new WaitForSeconds(_timeBeforeExecution); //wait till back in 

        anim.enabled = false;
        giantAnim.enabled = true;
        Instantiate(_disappearVFX, _superGiant.transform.position, Quaternion.identity);
        _superGiant.SetActive(false);
        AudioManager._instance.PlayAtRandomPitch("BloodSplatterSFX");
    }

    private void BeginBothDefeated()
    {
        StopAllCoroutines();
        StartCoroutine(BothDefeated());

    }

    private IEnumerator BothDefeated()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait till back in game
        }
        yield return new WaitForSeconds(_timeBeforeExecution); //wait till back in 

        anim.enabled = false;
        Instantiate(_disappearVFX, _superGiant.transform.position, Quaternion.identity);
        _superGiant.SetActive(false);
        Instantiate(_disappearVFX, giantAnim.transform.position, Quaternion.identity);
        giantAnim.gameObject.SetActive(false);
        AudioManager._instance.PlayAtRandomPitch("BloodSplatterSFX");
    }

    private void PlayerPassed()
    {
        if (_playerTrans != false)
        {
            _playerTrans.position = _escapePoint.position;
        }
    }


    public override void ResetNPC()
    {
        base.ResetNPC();
        SetUp();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (isInitiated)
        {
            DecisionManager._instance.GetConsequenceByName(_defeatBothCon).OnConsequenceTriggered -= BeginBothDefeated;
            DecisionManager._instance.GetConsequenceByName(_defeatBigCon).OnConsequenceTriggered -= BeginBigGiantDefeated;
            DecisionManager._instance.GetConsequenceByName(_escapeCon).OnConsequenceTriggered -= PlayerPassed;
        }

    }


    protected override void ActivateComponents()
    {
        //deactivate when not needed  improve performance
        _superGiant.SetActive(true);
        _smallGiant.SetActive(true);
        anim.enabled = true;

    }

    protected override void DeactivateComponents()
    {
        //Activate when needed  improve performance
        _superGiant.SetActive(false);
        _smallGiant.SetActive(false);
        anim.enabled = false;
    }
}
