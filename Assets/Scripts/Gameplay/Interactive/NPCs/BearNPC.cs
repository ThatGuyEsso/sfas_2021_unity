﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearNPC : BaseNPC
{
    [SerializeField] private int _vendingMachineBeatID;
    [SerializeField] private GameObject _gfx;
    [SerializeField] private List<string> _passConsequences;
    protected override void Awake()
    {
        base.Awake();
        //In event this consequence triggers change beat next beat ID
        DecisionManager._instance.GetConsequenceByName(_consequenceTrigger).OnConsequenceTriggered += GoToVendingMachinedBeat;

        foreach(string consequence in _passConsequences)
        {
            DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered += LetPlayerPass;
        }
    }


    //On enter and exit event checks if it is player
    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            _inRange = true;
            HUDManager._instance.prompt.DisplayText(_promptText);
        }
    }
    protected void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            HUDManager._instance.prompt.HideText();
            _inRange = false;
        }
    }
    protected void GoToVendingMachinedBeat()
    {
        _nextBeatID = _vendingMachineBeatID;
    }

    private void LetPlayerPass()
    {
        _isActivated = true;
        _gfx.SetActive(false);
    }
    public override void ResetNPC()
    {
        base.ResetNPC();
        _gfx.SetActive(true);
    }
}
