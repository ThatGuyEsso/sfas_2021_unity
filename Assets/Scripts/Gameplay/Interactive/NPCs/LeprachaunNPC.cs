﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeprachaunNPC : BaseNPC
{
    [SerializeField] private int _stolenGoldBeatID;
    [SerializeField] protected List<string> _hideTriggers;//trigger that causes npc to point to a new beat
    [SerializeField] protected string _SFXtrigger;//trigger that causes npc to point to a new beat
    [SerializeField] private GameObject _gfx;
    [SerializeField] private float _holdBeforeDisappear;
    [SerializeField] [Range(0f,10f)]private float _minTimeToSigh;
    [SerializeField] [Range(0f, 10f)] private float _maxTimeToSigh;

    protected bool _shouldPlaySFX;
     private float _currTimeToSigh;
    protected override void Awake()
    {
        base.Awake();
        //In event this consequence triggers change beat next beat ID
        DecisionManager._instance.GetConsequenceByName(_consequenceTrigger).OnConsequenceTriggered += GoToStolenGoldBeat;
        DecisionManager._instance.GetConsequenceByName(_SFXtrigger).OnConsequenceTriggered += EnableSFX;
        _currTimeToSigh = 0;
        _shouldPlaySFX = false;
        foreach (string con in _hideTriggers)
        {
            DecisionManager._instance.GetConsequenceByName(con).OnConsequenceTriggered += BeginHideGFX;
        }
    }

    protected override void Update()
    {
        if (GenreManager.IsFirstPersonAdventure() && _inRange)
        {
            if (_inRange && !_isActivated)
            {
                UpdateInput();
                if (_shouldPlaySFX)
                {

                    PlaySighSFX();
                }
            }
        }
    }

    //On enter and exit event checks if it is player
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            _inRange = true;
            HUDManager._instance.prompt.DisplayText(_promptText);
        }
    }
    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            HUDManager._instance.prompt.HideText();

            _inRange = false;
        }
    }

    private void BeginHideGFX()
    {
        StartCoroutine(HideGFX());
    }
    private IEnumerator HideGFX()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait till back in game
        }
        yield return new WaitForSeconds(_holdBeforeDisappear); //wait till back in game
        Instantiate(_disappearVFX, _gfx.transform.position, Quaternion.identity);
        _gfx.SetActive(false);
    }
        //Change next Id to respective beat
    protected void GoToStolenGoldBeat()
    {
        _nextBeatID = _stolenGoldBeatID;
        
    }

    public override void ResetNPC()
    {
        base.ResetNPC();
        _gfx.SetActive(true);
        _shouldPlaySFX = false;
    }

    void PlaySighSFX()
    {
        if(_currTimeToSigh <= 0f)
        {
            AudioManager._instance.PlayAtRandomPitch("SighSFX");
            _currTimeToSigh = Random.Range(_minTimeToSigh, _maxTimeToSigh);
        }
        else
        {
            _currTimeToSigh -= Time.deltaTime;
        }
    }

    private void EnableSFX()
    {
        _shouldPlaySFX = true;
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        foreach (string con in _hideTriggers)
        {
            DecisionManager._instance.GetConsequenceByName(con).OnConsequenceTriggered -= BeginHideGFX;
            DecisionManager._instance.GetConsequenceByName(_consequenceTrigger).OnConsequenceTriggered -= GoToStolenGoldBeat;
        }
    }
    protected override void UpdateInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //trigger

            HUDManager._instance.prompt.HideText();
            BeginTransition();
            _shouldPlaySFX = false;
        }

    }
        


}
