﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoonCharacter : MonoBehaviour
{
    private Animator _animController;
    [SerializeField] private string _guardPose;
    [SerializeField] private string _relaxedPose;

    [SerializeField] private Transform _clearPathTransform;
    private Transform _initTransform;
    private void Awake()
    {
        _animController = gameObject.GetComponent<Animator>();
        _initTransform = transform;

        PlayGuardPose();

    }


    public void ClearPath()
    {
        transform.position = _clearPathTransform.position;
        transform.rotation = _clearPathTransform.rotation;
        PlayRelaxedPose();
    }
    private void PlayGuardPose()
    {
        _animController.Play(_guardPose);
    }

    private void PlayRelaxedPose()
    {
        _animController.Play(_relaxedPose);
    }


    public void ResetCharacter()
    {
        transform.localPosition = _initTransform.position;
        transform.rotation = _initTransform.rotation;
        PlayGuardPose();
    }
}
