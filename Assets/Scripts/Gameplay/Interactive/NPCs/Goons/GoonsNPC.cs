﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoonsNPC : LeprachaunNPC
{
    [SerializeField] private GoonCharacter[] _goons;
    [SerializeField] private List<string> _clearConsequences;
    protected override void Awake()
    {
        _nextBeatID = _defaultBeatID;
        DecisionManager._instance.GetConsequenceByName(_consequenceTrigger).OnConsequenceTriggered += GoToStolenGoldBeat;

        foreach(string con in _clearConsequences)
        {
            DecisionManager._instance.GetConsequenceByName(con).OnConsequenceTriggered += ClearPath;
        }
    }

    //On enter and exit event checks if it is player
    protected override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            _inRange = true;
            HUDManager._instance.prompt.DisplayText(_promptText);
        }
    }
    protected override void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            HUDManager._instance.prompt.HideText();
            _inRange = false;
        }
    }


    private void ClearPath()
    {
        foreach (GoonCharacter goon in _goons)
        {
            goon.ClearPath();
        }
    }
    public override void ResetNPC()
    {
        _isActivated = false;
        _inRange = false;
        _shouldPlaySFX = false;
        foreach (GoonCharacter goon in _goons)
        {
            goon.ResetCharacter();
        }
    }

}
