﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrollEmergeTrigger : MonoBehaviour
{
    public event EmergeDelegate OnEmergeTrigger;
    public delegate void EmergeDelegate();
    private bool isTriggered;


    private void OnTriggerEnter(Collider other)
    {
        if (!isTriggered)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                isTriggered = true;
                OnEmergeTrigger?.Invoke();//trigger delegate
            }
        }
    }


    public void ResetTrigger()
    {
        isTriggered = false;
    }
}
