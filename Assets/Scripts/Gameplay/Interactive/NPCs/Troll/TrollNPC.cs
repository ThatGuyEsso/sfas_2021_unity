﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrollNPC : BaseNPC
{
    [SerializeField] private GameObject _gfx;
    [SerializeField] private GameObject _emergeVFX;
    [SerializeField] private float _emergeRate;
    [SerializeField] private float _emergeDistance;
    [SerializeField] private float _holdSecsBefSlay;
    [SerializeField] private string _correctAnswerCon;
    [SerializeField] private string _slayTrollCon;

    [SerializeField] private Transform particlePos;
    [SerializeField] private bool _isEmergeed;
    [SerializeField] private TrollEmergeTrigger _emergeTrigger;
    private

    Vector3 initPos;

    protected override void Awake()
    {
        base.Awake();
        initPos = transform.position;
        SetUp();
    }
    void SetUp()
    {
    
       
        transform.position = initPos + new Vector3(0f, initPos.y - _emergeDistance, 0.0f);
       
  
        _gfx.SetActive(false);
        _isEmergeed = false;
        DecisionManager._instance.GetConsequenceByName(_correctAnswerCon).OnConsequenceTriggered += BeginDisappearance;
        DecisionManager._instance.GetConsequenceByName(_slayTrollCon).OnConsequenceTriggered += BeginSlayTroll;
        if (_emergeTrigger != false)
        {
            _emergeTrigger.OnEmergeTrigger += BeginEmerge;
        }
    }


    //On enter and exit event checks if it is player
    protected void OnTriggerEnter(Collider other)
    {
        if (_isEmergeed)
        {

            if (other.gameObject.CompareTag("Player") && _isActivated == false)
            {
                _inRange = true;
                HUDManager._instance.prompt.DisplayText(_promptText);
            }
        }
    }
    protected void OnTriggerExit(Collider other)
    {
        if (_isEmergeed)
        {

            if (other.gameObject.CompareTag("Player") && _isActivated == false)
            {
                HUDManager._instance.prompt.HideText();
                _inRange = false;
            }
        }
    }
    private void BeginSlayTroll()
    {
        StopAllCoroutines();
        StartCoroutine(SlayTroll());
    }

    private IEnumerator SlayTroll()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait till back in game
        }
        yield return new WaitForSeconds(_holdSecsBefSlay) ; //wait till back in game

        AudioManager._instance.PlayAtRandomPitch("BloodSplatterSFX");
        Instantiate(_disappearVFX, _gfx.transform.position +Vector3.up, Quaternion.identity);
        _gfx.SetActive(false);
    }
    private void BeginEmerge()
    {
        StopAllCoroutines();
        _gfx.SetActive(true);
        StartCoroutine(Emerge());
    }

    private IEnumerator Emerge()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait t
        }

        if (!_isEmergeed)
        {
            CamShake._instance.StartScreenShake(0.15f, 0.015f);
            AudioManager._instance.PlayAtRandomPitch("RumbleSFX");
            LoopingParticleCleanUp vfx = Instantiate(_emergeVFX, particlePos.position,Quaternion.identity).GetComponent<LoopingParticleCleanUp>();


            float currY = transform.position.y;
            float endY = transform.position.y + _emergeDistance;
            while (currY < endY)
            {
                currY += _emergeDistance/100f;
                transform.position = new Vector3(transform.position.x, currY, transform.position.z);
                yield return new WaitForSeconds(_emergeRate);
            }
            transform.position = new Vector3(transform.position.x, endY, transform.position.z);
            _isEmergeed = true;
            vfx.CleanUpVFX();



            CamShake._instance.EndScreenShake(0.0125f);
            AudioManager._instance.BeginFadeOut("RumbleSFX");
        }

    }
    private IEnumerator Disappear()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait t
        }

        if (_isEmergeed)
        {
          
            float currY = transform.position.y;
            float endY = transform.position.y - _emergeDistance;
            LoopingParticleCleanUp vfx = Instantiate(_emergeVFX, particlePos.position, Quaternion.identity).GetComponent<LoopingParticleCleanUp>();
            while (currY > endY)
            {
                currY -= _emergeDistance / 100f;
                transform.position = new Vector3(transform.position.x, currY, transform.position.z);
                yield return new WaitForSeconds(_emergeRate);
            }
            _isEmergeed = false;

            vfx.CleanUpVFX();
            _gfx.SetActive(false);
        }

    }
    private void BeginDisappearance()
    {
        StopAllCoroutines();
        StartCoroutine(Disappear());
     
    }

    //Incase of restart reset game
    public override void ResetNPC()
    {
        base.ResetNPC();
        _emergeTrigger.ResetTrigger();
        SetUp();
    }

    //Clear up references when destroyed to stop nullptr errors
    protected override void OnDestroy()
    {
        DecisionManager._instance.GetConsequenceByName(_correctAnswerCon).OnConsequenceTriggered -= BeginDisappearance;
        DecisionManager._instance.GetConsequenceByName(_slayTrollCon).OnConsequenceTriggered -= BeginSlayTroll;
        if (_emergeTrigger != false)
        {
            _emergeTrigger.OnEmergeTrigger -= BeginEmerge;
        }
    }
}
