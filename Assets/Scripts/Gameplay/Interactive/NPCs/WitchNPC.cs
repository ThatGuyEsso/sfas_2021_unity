﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchNPC : BaseNPC
{
    [SerializeField] private GameObject _beggarGFX;
    [SerializeField] private GameObject _witchGFX;
    [SerializeField] private string _curseConsequences;
    [SerializeField] private string _leaveConsequences;
    [SerializeField] private string _blessingConsequences;
    [SerializeField] private string _activationTrigger;
    [SerializeField] private float _timebeforeDisappearance =2.5f;
    [SerializeField] [Range(0,5)] private float _minTimeToBeg = 0f;
    [SerializeField] [Range(0, 5)] private float _maxTimeToBeg = 10;
    private bool _shouldPlaySFX;
    private float _currTimeToBeg;
    AudioSource _currBegSFX;
    [SerializeField] private GameObject _blessingVFX;
    protected override void Awake()
    {
        base.Awake();
        SetUp();
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            _inRange = true;
            HUDManager._instance.prompt.DisplayText(_promptText);
            _shouldPlaySFX = true;

        }
    }
    protected void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isActivated == false)
        {
            HUDManager._instance.prompt.HideText();
            Debug.Log("Witch called hide text");
            _inRange = false;
            _shouldPlaySFX = false;

        }
    }

    private void SetUp()
    {

        isInitiated = true;
        _shouldPlaySFX = false;
        if (!_beggarGFX.activeSelf) _beggarGFX.SetActive(true);
        if (_witchGFX.activeSelf) _witchGFX.SetActive(false);

        DeactivateComponents();
        DecisionManager._instance.GetConsequenceByName(_activationTrigger).OnConsequenceTriggered += ActivateComponents;
        DecisionManager._instance.GetConsequenceByName(_curseConsequences).OnConsequenceTriggered += WitchCurse;
        DecisionManager._instance.GetConsequenceByName(_leaveConsequences).OnConsequenceTriggered += LeavePlayer;
        DecisionManager._instance.GetConsequenceByName(_blessingConsequences).OnConsequenceTriggered += BlessPlayer;

    }

    protected override void Update()
    {
        base.Update();

        if (_shouldPlaySFX)
        {
            PlayBeggarSFX();
        }
    }
    private void WitchCurse()
    {
        AudioManager._instance.Stop("BeggarSFX");
        if (_beggarGFX.activeSelf) _beggarGFX.SetActive(false);
        if (!_witchGFX.activeSelf) _witchGFX.SetActive(true);
        _shouldPlaySFX = false;
        StartCoroutine(BeginToHideWitchGFX());
        _isActivated = true;
    }

    private void LeavePlayer()
    {
        AudioManager._instance.Stop("BeggarSFX");
        if (_beggarGFX.activeSelf) _beggarGFX.SetActive(false);
        if (_witchGFX.activeSelf) _witchGFX.SetActive(false);
        _shouldPlaySFX = false;
        _isActivated = true;
    }

    private void BlessPlayer()
    {

        AudioManager._instance.Stop("BeggarSFX");
        _isActivated = true;

        _shouldPlaySFX = false;
        StartCoroutine(BeginToHideWitchGFX());
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (isInitiated)
        {
            DecisionManager._instance.GetConsequenceByName(_curseConsequences).OnConsequenceTriggered -= WitchCurse;
            DecisionManager._instance.GetConsequenceByName(_leaveConsequences).OnConsequenceTriggered -= LeavePlayer;
            DecisionManager._instance.GetConsequenceByName(_blessingConsequences).OnConsequenceTriggered -= BlessPlayer;
        }

    }


    IEnumerator BeginToHideWitchGFX()
    {
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null;
        }
        if (DecisionManager._instance.GetConsequenceByName(_curseConsequences).IsTriggered)
        {
            Instantiate(_disappearVFX, _witchGFX.transform.position, Quaternion.identity);
        }
        else
        {
            Instantiate(_blessingVFX, _witchGFX.transform.position, Quaternion.identity);
        }
        if (_beggarGFX.activeSelf) _beggarGFX.SetActive(false);
        if (!_witchGFX.activeSelf) _witchGFX.SetActive(true);

        yield return new WaitForSeconds(_timebeforeDisappearance);
        if (DecisionManager._instance.GetConsequenceByName(_curseConsequences).IsTriggered)
        {
            AudioManager._instance.Play("WitchLaugh");
        }
        Instantiate(_disappearVFX, transform.position, Quaternion.identity);
        if (_witchGFX.activeSelf) _witchGFX.SetActive(false);
    }


    private void PlayBeggarSFX()
    {
        
        
            if(_currBegSFX != false)
            {
                if (_currBegSFX.isPlaying) return;
            }
            if (_currTimeToBeg <= 0f)
            {
                _currBegSFX = AudioManager._instance.PlayRandFromGroup("BeggarSFX");
                _currTimeToBeg = Random.Range(_minTimeToBeg, _maxTimeToBeg);
            }
            else
            {
                _currTimeToBeg -= Time.deltaTime;
            }
    }

    public override void ResetNPC()
    {
        base.ResetNPC();
        _shouldPlaySFX =false;
    }
    protected override void ActivateComponents()
    {
        //deactivate when not needed  improve performance
        _beggarGFX.SetActive(true);
  



    }

    protected override void DeactivateComponents()
    {
        //Activate when needed  improve performance
        _beggarGFX.SetActive(false);
        _witchGFX.SetActive(false);
        _shouldPlaySFX = false;
   
    }
}
