﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminalTransition : MonoBehaviour
{
    [SerializeField] private int beatID;
    private bool isInRange =false;//player is in trigger volume
    private bool isActivated = false;//haven't already been used
    private bool isInitiated =false;
    public void ResetTerminal()
    {
        isActivated = false;
        isInRange = false;
    }

    private  void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.Resetting:
                ResetTerminal();
                break;
        }
    }
    private void Awake()
    {
        isInitiated = true;
        GameStateManager._instance.OnStateChange += EvaluateNewState;
    }
    void Update()
    {
        if (GenreManager._currGenre == Genres.FirstPersonAdeventure && isActivated ==false)
        {
            UpdateInput();
        }
    }

    //On enter and exit event checks if it is player
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")&& isActivated == false)
        {
            isInRange = true;
            HUDManager._instance.prompt.DisplayText("[E] To Access Termininal");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player")&& isActivated==false)
        {
            HUDManager._instance.prompt.HideText();
            isInRange = false;
        }
    }

    private void UpdateInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //only trigger if in range
            if (isInRange && !isActivated)
            {
                AudioManager._instance.PlayAtRandomPitch("MenuSelectSFX");
                HUDManager._instance.prompt.HideText();
                BeginTransition();

            }
        }
    }


    private void BeginTransition()
    {
        GenreManager._instance.ChangeGenre(Genres.TextAdventure);
        GenreManager._instance.BeginNewStoryBeat(beatID);
        isActivated = true;
    }

    private void OnDestroy()
    {
        if (isInitiated)
        {
            GameStateManager._instance.OnStateChange -= EvaluateNewState;
        }
  
    }
}
