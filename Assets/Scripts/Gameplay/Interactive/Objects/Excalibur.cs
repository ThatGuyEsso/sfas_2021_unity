﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Excalibur : BaseInteractableObject
{
    enum ExcaliburWorth
    {
        Worthy,
        Unworthy,
        Unholy
    };

    ExcaliburWorth _playersWorth;
    [SerializeField] private int _smiteBeatID;
    [SerializeField] private float _timeBeforeTransition;
    protected override void Awake()
    {
        base.Awake();
    }


    protected override void UpdateInput()
    {
        base.UpdateInput();//returns if not first person mode or interactable
        if (Input.GetKeyDown(KeyCode.E))
        {
            switch (_playersWorth)
            {
                case ExcaliburWorth.Worthy:

                    PullExcalibur();
                    break;
                case ExcaliburWorth.Unholy:
                    BeginPlayerSmite();
                    break;
                case ExcaliburWorth.Unworthy:
                    FailToPullExcalibur();
                   

                    break;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isInteractable == true)
        {
            _inRange = true;
         
            HUDManager._instance.prompt.DisplayText(_promptText);
        

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            HUDManager._instance.prompt.ResetPrompt();
            HUDManager._instance.prompt.HideText();
            _inRange = false;
        }
    }


    void EvaluatePlayerWorth()
    {
        //If player is blessed by the leprachaun or witch he is worthy
        if(DecisionManager._instance.GetConsequenceByName("Leprachaun Blessing").IsTriggered||
            DecisionManager._instance.GetConsequenceByName("Good Elixir").IsTriggered)
        {
            _playersWorth = ExcaliburWorth.Worthy;
        }
        //if player has been cursed in anways he is unholy
        else if (DecisionManager._instance.GetConsequenceByName("Witch's Curse").IsTriggered ||
            DecisionManager._instance.GetConsequenceByName("Leprachaun Curse").IsTriggered)
        {
            _playersWorth = ExcaliburWorth.Unholy;
        }
        //otherwise the player is just not worthy
        else
        {
            _playersWorth = ExcaliburWorth.Unworthy;
        }
    }

    private void PullExcalibur()
    {
        MusicManager._instance.BeginHoldSong(5.0f);
        DecisionManager._instance.GetConsequenceByName(_triggerConsequences[0]).TriggerConsequence();
        DecisionTracker._instance.StoreDecision(_triggerConsequences[0]);
        HUDManager._instance.prompt.ResetPrompt();
        AudioManager._instance.Play("ThunderSFX");
        HUDManager._instance.prompt.DisplayText("YOU HAVE ACQUIRED THE POWER OF EXCALIBUR");
        _objectGFX.SetActive(false);
    }

    private void FailToPullExcalibur()
    {
        DecisionManager._instance.GetConsequenceByName(_triggerConsequences[1]).TriggerConsequence();
        DecisionTracker._instance.StoreDecision(_triggerConsequences[1]);
        HUDManager._instance.prompt.SetTextColour(Color.red);
        CamShake._instance.DoScreenShake(0.25f, 0.2f, 0.1f, 0.05f);
        _isInteractable = false;
        HUDManager._instance.prompt.DisplayText("You're not worthy to wield EXCALIBUR");

    }
    private void BeginPlayerSmite()
    {
        MusicManager._instance.StopMusic();
        LoadingScreen._instance.OnFadeEnd += BeginFlash;
        AudioManager._instance.Play("ThunderSFX");
        LoadingScreen._instance.FadeInBackground(Color.white, 0.005f);
        DecisionManager._instance.GetConsequenceByName(_triggerConsequences[2]);
        CamShake._instance.DoScreenShake(0.25f, 0.15f, 0.1f, 0.05f);
        _isInteractable = false;
    }


    private void BeginFlash()
    {
        LoadingScreen._instance.OnFadeEnd -= BeginFlash;
        StopAllCoroutines();
        StartCoroutine(Flash());
      

    }

    private IEnumerator Flash()
    {
        yield return new WaitForSeconds(_timeBeforeTransition);
        LoadingScreen._instance.OnFadeEnd += GoToSmiteBeat;
        LoadingScreen._instance.BeginFadeOutBackground(0.01f);
        
    }
    private void GoToSmiteBeat()
    {
        LoadingScreen._instance.OnFadeEnd -= GoToSmiteBeat;
        GenreManager._instance.DirectlySetGenre(Genres.TextAdventure);
        GenreManager._instance.BeginNewStoryBeat(_smiteBeatID);
    }

    override protected void BindToConsequences()
    {
        foreach (string consequence in _activationConsequences)
        {
            //bind become active an evaluation on when computer starts
            DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered += BecomeInteractable;
            DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered += EvaluatePlayerWorth;
        }

    }


    public override void ResetObject()
    {
        base.ResetObject();
        _objectGFX.SetActive(true);
    }
}
