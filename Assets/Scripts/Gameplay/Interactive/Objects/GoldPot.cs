﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GoldPot : BaseInteractableObject
{
    [SerializeField] private GameObject _hiddenGoldGFX;
    [SerializeField] private Vector3 _initialSpot;
    [SerializeField] private float _timeBeforeEvent;
    [SerializeField] private GameObject _eventVFX;
    //Enum to handle what decision player made relative to gold
    private enum GoldDecision
    {
        Leave,
        Steal,
        Hide
    };
    GoldDecision _decision = GoldDecision.Leave;

    protected override void Awake()
    {
        _initialSpot= transform.position;
        base.Awake();
     
  
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isInteractable == true)
        {
            _inRange = true;
            switch (_decision)
            {
                case GoldDecision.Hide:
                    HUDManager._instance.prompt.DisplayText("[E] To Hide Gold");
                    break;
                case GoldDecision.Steal:
                    HUDManager._instance.prompt.DisplayText("[E] To Steal Gold");
                    break;
            }
        
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            HUDManager._instance.prompt.HideText();
            _inRange = false;
        }
    }


    protected override void UpdateInput()
    {
        base.UpdateInput();//returns if not first person mode or interactable
        if (Input.GetKeyDown(KeyCode.E))
        {
            switch(_decision)
            {
                case GoldDecision.Hide:
                    HUDManager._instance.prompt.HideText();
                    Debug.Log("Gold called hide text");
                    StartCoroutine(HideGold());
                    break;
                case GoldDecision.Steal:
                    HUDManager._instance.prompt.HideText();
                    Debug.Log("Gold called hide text");
                    StartCoroutine( StealGold());
                    break;
            }
        }
    }


    private IEnumerator HideGold()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait till back in game
        }
        _isInteractable = false;
        yield return new WaitForSeconds(_timeBeforeEvent); //wait before executing event
        //execute decision
        Instantiate(_eventVFX, _initialSpot, Quaternion.identity);
        _objectGFX.SetActive(false);
        DecisionManager._instance.GetConsequenceByName("Hide Gold").TriggerConsequence();
        DecisionTracker._instance.StoreDecision("Hide Gold");
        yield return new WaitForSeconds(0.5f);
        Instantiate(_eventVFX, _hiddenGoldGFX.transform.position, Quaternion.identity);
        _hiddenGoldGFX.SetActive(true);
        _isInteractable = false;
 
    }


    private IEnumerator StealGold()
    {
        //wait till back in first person mode
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null; //wait till back in game
        }
        _isInteractable = false;
        DecisionManager._instance.GetConsequenceByName("Steal Gold").TriggerConsequence();
        DecisionTracker._instance.StoreDecision("Steal Gold");
        yield return new WaitForSeconds(_timeBeforeEvent); //wait before executing event
        Instantiate(_eventVFX, _initialSpot, Quaternion.identity);
        _objectGFX.SetActive(false);//make not visible
       
  

    }

    private void StealGoldDecision()
    {
        _decision = GoldDecision.Steal;
        _isInteractable = true;
    }

    private void HideGoldDecision()
    {

        _decision = GoldDecision.Hide;
        _isInteractable = true;
    }


    override protected void BindToConsequences()
    {
        foreach (string consequence in _activationConsequences)
        {
            if(consequence == "Begin To Steal Gold")
            {
                DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered += StealGoldDecision;
            }else if(consequence == "Begin To Hide Gold")
            {
                DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered += HideGoldDecision;
            }
        
        }
    }

    public override void ResetObject()
    {
        base.ResetObject();
        _objectGFX.SetActive(true);
        _hiddenGoldGFX.SetActive(false);
        _isInteractable = false;
        transform.position = _initialSpot;
    }


    //protected override void OnDisable()
    //{
    //    base.OnDisable();
    //    foreach (string consequence in _activationConsequences)
    //    {
    //        if (consequence == "Begin To Steal Gold")
    //        {
    //            DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered -= StealGoldDecision;
    //        }
    //        else if (consequence == "Begin To Hide Gold")
    //        {
    //            DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered -= HideGoldDecision;
    //        }

    //    }
    //}

    protected override void OnDestroy()
    {
        base.OnDestroy();
        foreach (string consequence in _activationConsequences)
        {
            if (consequence == "Begin To Steal Gold")
            {
                DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered -= StealGoldDecision;
            }
            else if (consequence == "Begin To Hide Gold")
            {
                DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered -= HideGoldDecision;
            }

        }
    }
}

