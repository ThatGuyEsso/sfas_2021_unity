﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendingMachine : BaseInteractableObject
{


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && _isInteractable == true)
        {
            _inRange = true;
        
            HUDManager._instance.prompt.DisplayText(_promptText);
      
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            HUDManager._instance.prompt.HideText();
            HUDManager._instance.prompt.ResetPrompt();
            _inRange = false;
        }
    }

    override protected void UpdateInput()
    {
        base.UpdateInput();
 
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (DecisionManager._instance.GetConsequenceByName("Hoover").IsTriggered)
            {
                foreach (string consequence in _triggerConsequences)
                {
                    DecisionManager._instance.GetConsequenceByName(consequence).TriggerConsequence();
                    DecisionTracker._instance.StoreDecision(consequence);
                }

                HUDManager._instance.prompt.DisplayText("A pasty falls out");
                _isInteractable = false;
            }
            else
            {
                HUDManager._instance.prompt.SetTextColour(Color.red);
                HUDManager._instance.prompt.DisplayText("You don't have any change");
                Debug.Log("Display no change");
                _isInteractable = false;
            }

        }
   
     

    }

    override public void ResetObject()
    {
        base.ResetObject();
    }

}
