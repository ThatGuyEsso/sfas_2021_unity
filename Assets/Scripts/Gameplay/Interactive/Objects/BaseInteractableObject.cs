﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class  BaseInteractableObject : MonoBehaviour
{

    [SerializeField] protected List<string> _triggerConsequences; //Consequence triggered when interacted with
    [SerializeField] protected GameObject _objectGFX;
    [SerializeField] protected List<string> _activationConsequences; //Consequence triggered when interacted with
    [SerializeField] protected string _promptText; //Consequence triggered when interacted with
    [SerializeField] protected bool _isInteractable;
    protected bool _inRange;
    protected bool isInitiatied =false;

    protected virtual void Awake()
    {
        ResetObject();
        BindToConsequences();
    }
    protected void TriggerConsequences()
    {
        foreach(string consequence in _triggerConsequences)
        {
            DecisionManager._instance.GetConsequenceByName(consequence).TriggerConsequence();
        }
       
    }

    virtual protected void Update()
    {
        if (_inRange)
        {
            UpdateInput();
        }

    }

    virtual public void ResetObject()
    {
        //each objects had different ways to reset
        _inRange = false;
        _isInteractable = false;

    }

    //Update input
    virtual protected void UpdateInput()
    {
        if (!_isInteractable || !GenreManager.IsFirstPersonAdventure()&&_inRange) return;
    }

    virtual public void BecomeInteractable()
    {
        _isInteractable = true;
    }

    //Set consequences the enables object
    virtual protected void BindToConsequences()
    {
        foreach (string consequence in _activationConsequences)
        {
            DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered += BecomeInteractable;
        }

    }

    virtual protected void OnDestroy()
    {
        if (!isInitiatied) return;
        foreach (string consequence in _activationConsequences)
        {
            DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered -= BecomeInteractable;
        }
    }

    //virtual protected void OnDisable()
    //{
    //    foreach (string consequence in _activationConsequences)
    //    {
    //        DecisionManager._instance.GetConsequenceByName(consequence).OnConsequenceTriggered -= BecomeInteractable;
    //    }
    //}
}
