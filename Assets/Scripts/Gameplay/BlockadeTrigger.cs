﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockadeTrigger : MonoBehaviour
{
    public event BlockadeTriggerDelegate OnTrigger;
    public delegate void BlockadeTriggerDelegate();


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            OnTrigger?.Invoke();
            //Debug.Log("Close barricade");
        }
    }
}
