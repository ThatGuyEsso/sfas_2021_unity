﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPointer : MonoBehaviour
{
    [SerializeField] private Vector3 offset;//keey offset to target position


    //change position of pointer
    public void UpdatePointerPosition(Vector3 newPosition)
    {
        transform.localPosition = newPosition + offset;
     
    }
}
