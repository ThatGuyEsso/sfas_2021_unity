﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSlider : MonoBehaviour
{
    [SerializeField] private float _maxValue;
    [SerializeField] private float _minValue;
    [SerializeField] private float _currentValue;
    [SerializeField] private GameObject _sliderBlockPrefab;
    [SerializeField] private Vector3 _sliderBlockOffset;
    [SerializeField] private Vector3 _sliderElementScale;
    [SerializeField] private List<GameObject> _sliderBlocks;


    public void SetUpSlider(float maxValue, float minValue, float curValue)
    {
        for(int i = 0; i < 10; i++)
        {
            _sliderBlocks.Add(Instantiate(_sliderBlockPrefab, transform));

            _sliderBlocks[i].transform.position = transform.position + i * _sliderBlockOffset;
            _sliderBlocks[i].transform.localScale = _sliderElementScale;
        }
        _maxValue = maxValue;
        _minValue = minValue;
        UpdateSlider(curValue);

    }


    public void UpdateSlider(float newValue)
    {
        //ensure new value us in range
        if (newValue < _minValue) _currentValue = _minValue;
        else if (newValue > _maxValue) _currentValue = _maxValue;
        else _currentValue = newValue;

        float percentChange = (_currentValue / _maxValue) ;

        int nActiveElements= Mathf.FloorToInt(_sliderBlocks.Count*percentChange);

        for (int i=0;i< _sliderBlocks.Count; i++)
        {
            //deactivate number of elements to that should not be displayed
            if(i<= nActiveElements-1)
            {
                _sliderBlocks[i].SetActive(true);
            }
            else
            {
                _sliderBlocks[i].SetActive(false);
            }
        }
        Debug.Log(nActiveElements);

    }

    public void DisplaySlider()
    {
        float percentChange = (_currentValue / _maxValue);

        int nActiveElements = Mathf.FloorToInt(_sliderBlocks.Count * percentChange);

        for (int i = 0; i < _sliderBlocks.Count; i++)
        {
            //deactivate number of elements to that should not be displayed
            if (i <= nActiveElements - 1)
            {
                _sliderBlocks[i].SetActive(true);
            }
            else
            {
                _sliderBlocks[i].SetActive(false);
            }
        }


    }


}
