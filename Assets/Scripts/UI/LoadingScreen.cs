﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    public static LoadingScreen _instance;
    //public GameObject cam;
    [SerializeField] private GameObject _loadingScreen;
    [SerializeField] private GameObject _loadingText;

    [SerializeField] private Image _loadingBackground;

    public event FadeEndDelegate OnFadeEnd;
    public delegate void FadeEndDelegate();
    private void Awake()
    {

        if (_instance == false)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
   
    }
        


    public void ToggleScreen( bool isOn)
    {

        _loadingText.SetActive(isOn);
        _loadingScreen.SetActive(isOn);
        _loadingBackground.gameObject.SetActive(isOn);
        if (isOn)
        {
            _loadingBackground.color = Color.black;
        }

    }

    public void FadeInBackground(Color color, float rate)
    {
        _loadingScreen.SetActive(true);
        _loadingBackground.gameObject.SetActive(true);
        _loadingBackground.color = color;
        _loadingText.SetActive(false);
        StartCoroutine(FadeInBackground(rate));
    }

    public void BeginFadeOutBackground(float rate)
    {
       
        StartCoroutine(FadeOutBackground(rate));
    }
    public void ToggleLoadingBackground()
    {
        if (_loadingBackground.gameObject.activeSelf)
        {
            _loadingBackground.gameObject.SetActive(false);
        }
        else
        {
            _loadingBackground.gameObject.SetActive(true);
        }
    }




    private IEnumerator FadeInBackground(float rate)
    {
        //enable if not true
        if (!_loadingBackground.gameObject.activeSelf) _loadingBackground.gameObject.SetActive(true);
        float oppacity = 0;
        _loadingBackground.color = new Color(_loadingBackground.color.r, _loadingBackground.color.g,
            _loadingBackground.color.g, oppacity);
        while(oppacity < 1.0f)
        {
            oppacity += 1.0f / 100f;
            _loadingBackground.color = new Color(_loadingBackground.color.r, _loadingBackground.color.g,
            _loadingBackground.color.g, oppacity);
            yield return new WaitForSeconds(rate);
        }
        _loadingBackground.color = new Color(_loadingBackground.color.r, _loadingBackground.color.g,
           _loadingBackground.color.g, 1.0f);
        OnFadeEnd?.Invoke();
    }

    private IEnumerator FadeOutBackground(float rate)
    {
        //enable if not true
        if (!_loadingBackground.gameObject.activeSelf) _loadingBackground.gameObject.SetActive(true);
        float oppacity = 1;
        _loadingBackground.color = new Color(_loadingBackground.color.r, _loadingBackground.color.g,
            _loadingBackground.color.g, oppacity);
        while (oppacity > 0f)
        {
            oppacity -= 1.0f / 100f;
            _loadingBackground.color = new Color(_loadingBackground.color.r, _loadingBackground.color.g,
            _loadingBackground.color.g, oppacity);
            yield return new WaitForSeconds(rate);
        }
        _loadingBackground.color = new Color(_loadingBackground.color.r, _loadingBackground.color.g,
           _loadingBackground.color.g, 0f);
        _loadingScreen.SetActive(false);
        OnFadeEnd?.Invoke();
    }







}



