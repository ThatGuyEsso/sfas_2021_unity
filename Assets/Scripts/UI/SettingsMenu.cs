﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenu : MonoBehaviour
{
    public static SettingsMenu _instance;
 
    [SerializeField] public static bool _isPaused;
    [SerializeField] private GameObject _settingsScreen;
    [SerializeField] private List<BaseSetting> _settings;

    [SerializeField] private ButtonPointer _pointer;
    private int _settingIndex;
    private void Awake()
    {
        //ensure singleton instance
        if (_instance == false)
        {
            _instance = this;

        }
        else
        {
            Destroy(gameObject);
        }

        foreach(BaseSetting setting in _settings)
        {
            setting.SetUp();
        }
        DontDestroyOnLoad(gameObject);
        _settingsScreen.SetActive(_isPaused);
    }



    private void Update()
    {
        //don't let player pause  during the the intro
        if(GameStateManager._currGameState!= GameState.InitIntro)
        {
            UpdateInput();
        }
      
    }


    private void UpdateInput()
    {
        //get escape input
        if (Input.GetKeyDown(KeyCode.P)) ToggleSettings();

        if (_isPaused)
        {
            //Move pointer upwards(decrease value)
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            {
                DecrementButtonIndex();
                _pointer.UpdatePointerPosition(_settings[_settingIndex].transform.localPosition);


            }
            //Move pointer downwards(increase value)
            else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                IncrementButtonIndex();
                _pointer.UpdatePointerPosition(_settings[_settingIndex].transform.localPosition);


            }

            //execute function of button currently being highlighted
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                TurnDownSetting();
            }else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)){
                TurnUpSetting();
            }
            
        }
    }

    public void ToggleSettings()
    {
        //inverse of what the previous version was
        _isPaused = !_isPaused;
        _settingsScreen.SetActive(_isPaused);
        if(_isPaused)
        {
            foreach (BaseSetting setting in _settings)
            {
                setting.Display();
            }
        }

        EvaluateSettings();
    }
    private void TurnUpSetting()
    {
        AudioManager._instance.PlayAtRandomPitch("MenuSelectSFX");
        _settings[_settingIndex].IncrementValue();
    }

    private void TurnDownSetting()
    {
        AudioManager._instance.PlayAtRandomPitch("MenuSelectSFX");
        _settings[_settingIndex].DecrementValue();
    }

    private void DecrementButtonIndex()
    {
        AudioManager._instance.PlayAtRandomPitch("MenuInteractSFX");
        _settingIndex--;
        //wrap around if min value
        if (_settingIndex < 0) _settingIndex = _settings.Count - 1;
    }
    private void IncrementButtonIndex()
    {
        AudioManager._instance.PlayAtRandomPitch("MenuInteractSFX");
        _settingIndex++;
        //wrap around if max value
        if (_settingIndex > _settings.Count - 1) _settingIndex = 0;
    }


    private void EvaluateSettings()
    {
        //if on pause game
        if (_isPaused) Time.timeScale = 0;
        else Time.timeScale = 1.0f; //else resume game
    }
}
