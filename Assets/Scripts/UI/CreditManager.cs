﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditManager : BaseManager
{
    [SerializeField] private List<CreditDisplay> _creditDisplays;
    private int _currDisplayIndex = 0;


    //public override void Init()
    //{
    //    BindToGameStateManager();


    //}
    private void Start()
    {
        DisplayNextDisplay();
    }


    private void DisplayNextDisplay()
    {
        //Bind to the initial value
        _creditDisplays[_currDisplayIndex].OnFinishDisplay += DisplayNextDisplay;
        //display current credit
        _creditDisplays[_currDisplayIndex].BeginDisplayCredit();
        _currDisplayIndex++;//increment to the next

        //if the next is the last display
        if (_currDisplayIndex >= _creditDisplays.Count - 1) _currDisplayIndex = _creditDisplays.Count - 1;
        //unbind from it as it will equal null
        _creditDisplays[_currDisplayIndex].OnFinishDisplay -= DisplayNextDisplay;
        if (_currDisplayIndex < 0) _currDisplayIndex = 0;
    }
    

  

    protected override void OnDestroy()
    {
        base.OnDestroy();
        foreach(CreditDisplay display in _creditDisplays)
        {
            display.OnFinishDisplay -= DisplayNextDisplay;
        }
    }



}
