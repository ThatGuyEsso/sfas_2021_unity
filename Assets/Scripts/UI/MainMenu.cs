﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] List<MenuButton> buttons;
    [SerializeField] private ButtonPointer pointer;
    private int buttonIndex;
    [SerializeField] private KeyCode primUp, secUp, primDown, secDown;
    private void Awake()
    {
        buttonIndex = 0;
        pointer.UpdatePointerPosition(buttons[buttonIndex].transform.localPosition);
    }
    private void Play()
    {
        //start game from the beginning

        TransitionManager._instance.StartGame();
    }
    private void TitleScreen()
    {
        //start game from the beginning

        GameStateManager._instance.BeginNewState(GameState.LoadTitleScreen);
    }

    private void Update()
    {
        //Only active if in the credit screen or game isn't paused
        if(GameStateManager._currGameState == GameState.TitleScreen && !SettingsMenu._isPaused 
            || GameStateManager._currGameState == GameState.CreditScreen&&!SettingsMenu._isPaused)
        {

             UpdateInput();
        }
    }
    private void Credits()
    {
        TransitionManager._instance.GoToCredits();
    }
    private void Quit()
    {
 
        Application.Quit();
    }


    private void UpdateInput()
    {
        //Move pointer upwards(decrease value)
        if(Input.GetKeyDown(primUp) || Input.GetKeyDown(secUp)){
            DecrementButtonIndex();
            pointer.UpdatePointerPosition(buttons[buttonIndex].transform.localPosition);
     
      
        }
        //Move pointer downwards(increase value)
        else if ( Input.GetKeyDown(primDown) || Input.GetKeyDown(secDown))
        {
            IncrementButtonIndex();
            pointer.UpdatePointerPosition(buttons[buttonIndex].transform.localPosition);
   

        }

        //execute function of button currently being highlighted
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EvaluateButtonPress();
        }
    }



  
    private void DecrementButtonIndex()
    {
        AudioManager._instance.PlayAtRandomPitch("MenuInteractSFX");
        buttonIndex--;
        //wrap around if min value
        if (buttonIndex < 0) buttonIndex = buttons.Count - 1;
    }
    private void IncrementButtonIndex()
    {
        AudioManager._instance.PlayAtRandomPitch("MenuInteractSFX");
        buttonIndex++ ;
        //wrap around if max value
        if (buttonIndex > buttons.Count - 1) buttonIndex = 0;
    }

    private void EvaluateButtonPress()
    {
        AudioManager._instance.PlayAtRandomPitch("MenuSelectSFX");
        //executte button function depending on button highlighted
        switch (buttons[buttonIndex].GetButtonType())
        {
            case ButtonType.Play:
                Play();
                break;
            case ButtonType.Credits:
                Credits();
                break;
            case ButtonType.Quit:
                Quit();
                break;
            case ButtonType.TitleScreen:
                TitleScreen();
                break;
        }
    }
}
