﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScreenBackground : MonoBehaviour
{
    [SerializeField]
    private float _fadeRate; //quickly oppacity changes eachtime

    [SerializeField]
    private Material _dissolveMat; //how much the oppacity changes eachtime

    public ScreenBackground _instance;
    private void Awake()
    {

        BindToGameStateManager();
    }

    public void Init()
    {
        //Bine genre evaluation the the change of genre
        GenreManager._instance.OnGenreChange += EvaluateNewGenre;
        GenreManager._instance.OnDirectChange += EvaluateNewDirectGenre;
        _dissolveMat.SetFloat("_DissolveAmount", 1f);


    }

    //To cover screen with out using a transition
    private void CoverScreen()
    {
        _dissolveMat.SetFloat("_DissolveAmount", 1.0f);
    }
    private void EvaluateNewGenre(Genres newGenre)
    {
        switch (newGenre)
        {
            case Genres.TextAdventure:
                BeginFadeIn();
                break;
            case Genres.FirstPersonAdeventure:
                BeginFadeOut();
                break;
        }
    }

    private void EvaluateNewDirectGenre(Genres newGenre)
    {
        switch (newGenre)
        {
            case Genres.TextAdventure:
                CoverScreen();
                break;
       
        }
    }
    public void BeginFadeIn()
    {
        StartCoroutine(FadeIn());
    }

    public void BeginFadeOut()
    {
        StartCoroutine(FadeOut());
    }


    //Bring the curtains down
    private IEnumerator FadeIn()
    {
        //make sure it has completely faded out
        float dissolveAmount = 0f;
        _dissolveMat.SetFloat("_DissolveAmount", dissolveAmount);

        //ensure dissolve colour  is visible
        _dissolveMat.SetColor("_OutlineCol", new Color(_dissolveMat.GetColor("_OutlineCol").r, _dissolveMat.GetColor("_OutlineCol").g, 
            _dissolveMat.GetColor("_OutlineCol").b, 1f));
        while (dissolveAmount < 1f)
        {
            dissolveAmount += 1f/100f;//increae oppacity
                                             //set new value of background to new value
            _dissolveMat.SetFloat("_DissolveAmount", dissolveAmount);
            yield return new WaitForSeconds(_fadeRate);//wait (to animate the fade in)
        }
        GenreManager._instance.SetCurrentGenre(Genres.TextAdventure);
    }
    //Bring the curtains Up
    private IEnumerator FadeOut()
    {
        //make sure it has completely faded in
        float dissolveAmount = 1f;
        _dissolveMat.SetFloat("_DissolveAmount", dissolveAmount);

        //ensure dissolve colour  is visible
        _dissolveMat.SetColor("_OutlineCol", new Color(_dissolveMat.GetColor("_OutlineCol").r, _dissolveMat.GetColor("_OutlineCol").g,
            _dissolveMat.GetColor("_OutlineCol").b, 1f));
        //
        while (dissolveAmount > 0f)
        {
            dissolveAmount -= 1f / 100f;//decrease oppacity
             //set new value of background to new value
            _dissolveMat.SetFloat("_DissolveAmount", dissolveAmount);
            yield return new WaitForSeconds(_fadeRate);//wait (to animate the fade in)
        }
        //remove dissolve outline
        _dissolveMat.SetColor("_OutlineCol", new Color(_dissolveMat.GetColor("_OutlineCol").r, _dissolveMat.GetColor("_OutlineCol").g,
          _dissolveMat.GetColor("_OutlineCol").b, 0f));

        GenreManager._instance.SetCurrentGenre(Genres.FirstPersonAdeventure);
    }



    public void BindToGameStateManager()
    {
        GameStateManager._instance.OnStateChange += EvaluateNewState;
    }

    protected void EvaluateNewState(GameState newState)
    {
        switch (newState)
        {
            case GameState.InitGame:
                Init();
                break;

            case GameState.Resetting:
                Init();
                break;
        }
    }


    private void OnDestroy()
    {
        StopAllCoroutines();
        //Unbind from genremanager
        GenreManager._instance.OnGenreChange -= EvaluateNewGenre;
        GenreManager._instance.OnDirectChange -= EvaluateNewDirectGenre;
        GameStateManager._instance.OnStateChange-= EvaluateNewState;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        //Unbind from genremanager
        GenreManager._instance.OnGenreChange -= EvaluateNewGenre;
        GenreManager._instance.OnDirectChange -= EvaluateNewDirectGenre;
        GameStateManager._instance.OnStateChange -= EvaluateNewState;
    }
}
