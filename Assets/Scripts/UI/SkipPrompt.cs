﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkipPrompt : MonoBehaviour
{
    private TextMeshProUGUI _skipPrompt;
    //private bool IsInTextMode;
    private void Awake()
    {
        _skipPrompt = gameObject.GetComponent<TextMeshProUGUI>();
        if (GenreManager.IsTextAdventure())  BeginFadeIn();
        GenreManager._instance.OnGenreChange += EvaluateNewGenre;
        GenreManager._instance.OnDirectChange += EvaluateNewGenre;
    }

    
    private void EvaluateNewGenre(Genres newGenre)
    {
        switch (newGenre)
        {
        
            case Genres.FirstPersonAdeventure:
                BeginFadeOut();
                break;
            case Genres.TextAdventure:
                BeginFadeIn();
                break;
        }
    }
    private void BeginFadeIn()
    {
        StopAllCoroutines();
        StartCoroutine(FadeIn());
    }


    private void BeginFadeOut()
    {
        StopAllCoroutines();
        StartCoroutine(FadeOut());
    }


    private IEnumerator FadeIn()
    {
        float oppacity = 0;
        _skipPrompt.color = new Color(_skipPrompt.color.r, _skipPrompt.color.g, _skipPrompt.color.b, oppacity);

        while (oppacity < 1f)
        {
            oppacity += 1f / 500f;
            _skipPrompt.color = new Color(_skipPrompt.color.r, _skipPrompt.color.g, _skipPrompt.color.b, oppacity);
            yield return null;
        }
        _skipPrompt.color = new Color(_skipPrompt.color.r, _skipPrompt.color.g, _skipPrompt.color.b, 1f);
    }

    private IEnumerator FadeOut()
    {
        float oppacity = 1;
        _skipPrompt.color = new Color(_skipPrompt.color.r, _skipPrompt.color.g, _skipPrompt.color.b, oppacity);

        while (oppacity > 0f)
        {
            oppacity -= 1f / 50f;
            _skipPrompt.color = new Color(_skipPrompt.color.r, _skipPrompt.color.g, _skipPrompt.color.b, oppacity);
            yield return null;
        }
        _skipPrompt.color = new Color(_skipPrompt.color.r, _skipPrompt.color.g, _skipPrompt.color.b, 0f);
    }

    private void OnDestroy()
    {
        GenreManager._instance.OnGenreChange -= EvaluateNewGenre;
        GenreManager._instance.OnDirectChange -= EvaluateNewGenre;
    }
}
