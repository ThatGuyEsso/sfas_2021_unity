﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CreditDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _creditTitle;
    [SerializeField] private TextMeshProUGUI _creditBody;
    private string _creditTitleText;
    private string _creditBodyText;

    [SerializeField] private float _timeBetweenLines;
    [SerializeField] private float _printRate;
    public event FinishDelegate OnFinishDisplay;
    public delegate void FinishDelegate();

    private void Awake()
    {
        Init();
    }
    public void Init()
    {
        //store initial text
        _creditTitleText = _creditTitle.text;
        _creditBodyText = _creditBody.text;

        //Clear displayed text 
        _creditTitle.text = string.Empty;
        _creditBody.text = string.Empty;
   
    }



    public void BeginDisplayCredit()
    {
        StartCoroutine(DisplayCredit());
    } 

    private IEnumerator DisplayCredit()
    {
        //Store characters of the text title and body
        char[] titleCharacters = _creditTitleText.ToCharArray();
        char[] BodyCharacters = _creditBodyText.ToCharArray();
        WaitForSeconds printRate = new WaitForSeconds(_printRate);
        //Print each character

        AudioManager._instance.PlayAtRandomPitch("TypingSFX");
        for (int i=0; i< titleCharacters.Length; i++)
        {
            _creditTitle.text += titleCharacters[i];
            yield return printRate;
        }
        AudioManager._instance.Stop("TypingSFX");
        //Wait some time before printing body
        yield return new WaitForSeconds(_timeBetweenLines);

        AudioManager._instance.PlayAtRandomPitch("TypingSFX");
        for (int i = 0; i < BodyCharacters.Length; i++)
        {
            _creditBody.text += BodyCharacters[i];
            yield return printRate;
        }
        AudioManager._instance.Stop("TypingSFX");
        yield return new WaitForSeconds(_timeBetweenLines);

        OnFinishDisplay?.Invoke();
    }

    private void OnDestroy()
    {
       if( AudioManager._instance.GetSound("TypingSFX").source.isPlaying)
        AudioManager._instance.Stop("TypingSFX");
    }

}
