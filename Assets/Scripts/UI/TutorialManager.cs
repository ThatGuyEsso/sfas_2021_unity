﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TutorialManager : MonoBehaviour
{
    [SerializeField] private RuntimeData _runtimeData;

    [SerializeField] private GameObject _mouseTutorial, _keyboardTutorial;
    [SerializeField] private float _fadeInRate, _fadeOutRate;
    [SerializeField] private float _maxBackgroundOppacity;
    [SerializeField] private float _displayDuration;
    [SerializeField] private List<Image> _keys;
    [SerializeField] private List<TextMeshProUGUI> _txtPrompt;
    [SerializeField] private List<Image> _backGrounds;


    private void Awake()
    {
        if (_runtimeData.isInitBoot)
        {
            BeginEnableTutorial();
            _runtimeData.isInitBoot = false;
           
        }
        else
        {
            ToggleTutorial(false);
        }
    }
    private void BeginEnableTutorial()
    {
        StopAllCoroutines();
        StartCoroutine(EnableTutorial());
    }
    private void BeginDisableTutorial()
    {
        StopAllCoroutines();
        StartCoroutine(DisableTutorial());
    }

    private void BreakFade(bool isFadingIn)
    {
        if (isFadingIn)
        {
            StopAllCoroutines();
            ToggleTutorial(true);
        }
        else
        {
            StopAllCoroutines();
            ToggleTutorial(false);
            GenreManager._instance.OnGenreChange -= EvaluateNewGenre;
        }
      
    }

    private void EvaluateNewGenre(Genres newGenre)
    {
        switch (newGenre)
        {
            case Genres.TextAdventure:
                BreakFade(false);
                break;
            case Genres.FirstPersonAdeventure:
                BreakFade(true);
                break;
        }
    }

    public IEnumerator EnableTutorial()
    {
      //set oppacity to zero to initialise fade in
        float oppacity = 0f;
        foreach(Image key in _keys)
        {
            key.color = new Color(key.color.r, key.color.g, key.color.b, oppacity);
        }
        foreach (Image backGround in _backGrounds)
        {
            backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, oppacity);
        }
        foreach (TextMeshProUGUI txt in _txtPrompt)
        {
            txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, oppacity);
        }

        //Wait till first person mode is enabled
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null;
        
        }
        //incase player activates termninal before fade out

        GenreManager._instance.OnGenreChange += EvaluateNewGenre;
        //start fading in tutorial prompts
        while (oppacity < 1f)
        {
            oppacity += 1f / 100f;
            foreach (Image key in _keys)
            {
                key.color = new Color(key.color.r, key.color.g, key.color.b, oppacity);
            }
            foreach (TextMeshProUGUI txt in _txtPrompt)
            {
                txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, oppacity);
            }

            //ensure background oppacity does not exceed given max
            if(oppacity <= _maxBackgroundOppacity)
            {
                foreach (Image backGround in _backGrounds)
                {
                    backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, oppacity);
                }
            }

            yield return new WaitForSeconds(_fadeInRate);
        }
  
        //at the end reset each value
        foreach (Image key in _keys)
        {
            key.color = new Color(key.color.r, key.color.g, key.color.b, 1f);
        }
        foreach (Image backGround in _backGrounds)
        {
            backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, _maxBackgroundOppacity);
        }
        foreach (TextMeshProUGUI txt in _txtPrompt)
        {
            txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, 1f);
        }

        Invoke("BeginDisableTutorial", _displayDuration);
    }
    public IEnumerator DisableTutorial()
    {
        //set oppacity to one to initialise fade in
        float oppacity = 1f;
        foreach (Image key in _keys)
        {
            key.color = new Color(key.color.r, key.color.g, key.color.b, oppacity);
        }
        foreach (Image backGround in _backGrounds)
        {
            backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, _maxBackgroundOppacity);
        }
        foreach (TextMeshProUGUI txt in _txtPrompt)
        {
            txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, oppacity);
        }

        //Wait till first person mode is enabled
        while (!GenreManager.IsFirstPersonAdventure())
        {
            yield return null;
        }

        //start fading in tutorial prompts
        while (oppacity > 0f)
        {
            //Simultaneously fade in image elements
            oppacity -= 1f / 100f;
            foreach (Image key in _keys)
            {
                key.color = new Color(key.color.r, key.color.g, key.color.b, oppacity);
            }
            foreach (TextMeshProUGUI txt in _txtPrompt)
            {
                txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, oppacity);
            }

            //ensure background oppacity does not exceed given max
            if (oppacity <= _maxBackgroundOppacity)
            {
                foreach (Image backGround in _backGrounds)
                {
                    backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, oppacity);
                }
            }

            yield return new WaitForSeconds(_fadeOutRate);
        }

        //at the end reset each value
        foreach (Image key in _keys)
        {
            key.color = new Color(key.color.r, key.color.g, key.color.b, 0f);
        }
        foreach (Image backGround in _backGrounds)
        {
            backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, 0f);
        }
        foreach (TextMeshProUGUI txt in _txtPrompt)
        {
            txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, 0f);
        }
        //disable elements not needed
        ToggleTutorial(false);
    }
    void ToggleTutorial(bool isOn)
    {
        if (!isOn)
        {
            if(_mouseTutorial.activeSelf) _mouseTutorial.SetActive(isOn);

            if (_keyboardTutorial.activeSelf) _keyboardTutorial.SetActive(isOn);
        }
        else
        {
            _mouseTutorial.SetActive(isOn);
            _keyboardTutorial.SetActive(isOn);

        }
    }



}
