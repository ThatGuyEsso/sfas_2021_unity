﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//All button types allows them to be evaluated
public enum ButtonType
{
    Play,
    Credits,
    TitleScreen,
    Quit
};
public class MenuButton : MonoBehaviour
{
    //set in inspector
    [SerializeField] private ButtonType buttonType;

    //return current type of button
    public ButtonType GetButtonType()
    {
        return buttonType;
    }
}
