﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TextPrompt : MonoBehaviour
{

    private TextMeshProUGUI _displayText;
    private string _currentText =string.Empty;
    private Color _defaultCol;
    private void Awake()
    {
        _displayText = gameObject.GetComponent<TextMeshProUGUI>();
        if (_displayText != false)
        {
            _displayText.text = _currentText;
            _defaultCol = _displayText.color;
        }
    }


    public void DisplayText(string newText)
    {
        _currentText = newText;
        _displayText.text = _currentText;
    }

    public void HideText()
    {
        _currentText = string.Empty;
        _displayText.text = _currentText;
    }

    public void SetTextColour(Color newColour)
    {
        _displayText.color = newColour;
    }

    public void ResetPrompt()
    {
        _currentText = string.Empty;
        _displayText.text = _currentText;
        _displayText.color = _defaultCol;
    }
}
