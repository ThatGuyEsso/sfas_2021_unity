﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CamShake : MonoBehaviour
{

   [SerializeField] private Transform initTrans;
  [Range(1.0f,4.0f) ] [SerializeField] private float resetScalar ;
    private CinemachineVirtualCamera vCamera;//Virtual camera
    //Time variables
    private float shakeTime;
    private float timeIn;
    private float timeOut;

    //Shake intensity variables
    private float startIntensity;
    private float currentIntensity;
    private float maxIntensity;
    //Component of shake noise
    private CinemachineBasicMultiChannelPerlin shakeNoise;
    private CinemachineBrain cameraBrain;
    public static CamShake _instance;

    private bool isShaking;
    public void Awake()
    {
        //Get cashe Refs
        vCamera = gameObject.GetComponent<CinemachineVirtualCamera>();
        startIntensity = vCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain;
        shakeNoise = vCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        CinemachineBrain.SoloCamera = vCamera;
        _instance = this;
    }


    public void Update()
    {
        //if shake time does not equal 0 shake should begin
        if (shakeTime > 0)
        {
            shakeTime -= Time.deltaTime;

            if (currentIntensity < maxIntensity)//If current magnitude is greater than max intensity stop increasing
            {
                //If there is fade in time
                if (timeIn > 0f)
                {
                    currentIntensity = Mathf.Lerp(startIntensity, maxIntensity, timeIn);
                    shakeNoise.m_AmplitudeGain = currentIntensity;
                    timeIn -= Time.deltaTime;

                }
                //If there is no fade in time
                else
                {
                    shakeNoise.m_AmplitudeGain = maxIntensity;
                }
            }


        }
        else
        {
            //If amplitude is not 0f
            if (shakeNoise.m_AmplitudeGain > 0&&!isShaking)
            {
                //if there is fade out time
                if (timeOut > 0f)
                {
                    //Stop shaking
                    shakeNoise.m_AmplitudeGain = Mathf.Lerp(currentIntensity, 0f, timeOut); ;
                    timeOut -= Time.deltaTime;

                }
                //if there is not fade out time
                else
                {
                    shakeNoise.m_AmplitudeGain = 0;
                }
            }

        }
    }
    //Set time variables which begins screenshake
    public void DoScreenShake(float duration, float magnitude, float timeSmoothIn, float timeSmoothOut)
    {
        shakeTime = duration;
        maxIntensity = magnitude;
        timeIn = timeSmoothIn;
        timeOut = timeSmoothOut;
        //Debug.Log("shake");
    }

    public void StartScreenShake( float magnitude, float timeSmoothIn)
    {
        if (!isShaking)
        {
            StopAllCoroutines();
            maxIntensity = magnitude;
            timeIn = timeSmoothIn;
            StartCoroutine(FadeIntoScreenShake());
        }
    }

    public void EndScreenShake(float timeSmoothOut)
    {
        if (isShaking)
        {
            StopAllCoroutines();
            timeOut = timeSmoothOut;
            StartCoroutine(FadeOutOfScreenShake());
        }
    }


    private IEnumerator FadeIntoScreenShake()
    {
        isShaking = true;
        float currMag =0;

        while(currMag < maxIntensity)
        {
            currMag += maxIntensity/100;
            shakeNoise.m_AmplitudeGain = currMag;
            yield return new WaitForSeconds(timeIn);
        }
    }
    private IEnumerator FadeOutOfScreenShake()
    {
      
        float currMag = shakeNoise.m_AmplitudeGain;

        while (currMag > 0f)
        {
            currMag -= maxIntensity / 100;
            shakeNoise.m_AmplitudeGain = currMag;
            yield return new WaitForSeconds(timeOut);
        }
        shakeNoise.m_AmplitudeGain = 0;
        isShaking = false;
        StartCoroutine(ReturnToInitPosition());
      
    }


    private IEnumerator ReturnToInitPosition()
    {
        float initDistance = Vector3.Distance(transform.position, initTrans.position);//Distance after shake finished
        float currDistance;//current distance between shake and initial position
        while (transform.position != initTrans.position)
        {

            transform.position = Vector3.Lerp(transform.position, initTrans.position, resetScalar * Time.deltaTime);//lerp to point
            currDistance = Vector3.Distance(transform.position, initTrans.position);//get new distance
            //if the distance between them is less that n% of the original then just make them equal
            if (currDistance / initDistance <= 0.01) transform.position = initTrans.position;
           yield return null;
        }
        Debug.Log("returned to init position");
    }

    private void OnDestroy()
    {
        CinemachineBrain.SoloCamera = null;
    }
}
